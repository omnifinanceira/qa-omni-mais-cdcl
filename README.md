# Automção Omni CDCL

Projeto para utilizar a Automação CDCL

## Pré-requisitos:
- [Ruby](https://cache.ruby-lang.org/pub/ruby/3.1/ruby-3.1.1.tar.gz "Ruby")
- [ChromeDriver](https://chromedriver.chromium.org/downloads "chromedriver")
* A versão do chromedriver baixada tem que ser compativel com a do seu Browser
* As configurações de ambiente necessarias precisam ser realizadas conforme este link: https://omnicfi.atlassian.net/l/c/bLu8QvWB

## Ferramentas utilizadas:
- [VSCode](https://code.visualstudio.com/ "VSCode")

## Tutorial, Instalação e Execução

### Executar este projeto em sua maquina

* Em um terminal, dentro da pasta 'tests' (C:\CDCL - Veri\qa-omni-mais-cdcl\tests) do projeto , execute o seguinte comando:

**Instalar a gem de gerenciamento:**  
```
gem install bundler
```

**Instalar as dependências:** 
```
bundler install
```

**Instalar uma nova gem ou versão de gem que foi declarada:** 
```
bundle update
```

### Comandos de execução:
O comando padrão utilizado para rodar um ou mais cenarios se da por: cucumber -v -t@nome_cenario.
Exemplo: Se o codigo estiver configurado com 3 cenarios destintos com a tag @nome_cenario, a automação executara todos, um em sequencia do outro.

Rodar cenários que possuam a string '@loja-seguro' no nome.
```
cucumber -v -t@loja-seguro
```

Rodar cenários que possuam a string '@premium' no nome.
```
cucumber -v -t@premium
```
* Sendo assim, basta passar a tag especifica do cenario que deseja rodar.

### Trabalhando com reports do Cucumber:  
Uma execução que gera reports do Cucumber irá criar uma pasta chamada 'reports' dentro da raiz do projeto. Se esta pasta existir e possuir arquivos válidos, você pode visualizar os resultados abrindo o arquivo html no navegador.

### Trabalhando com reports do Allure:  
Uma execução que gera reports do Allure irá criar seus arquivos dentro da pasta chamada 'reports_allure' na raiz do projeto.
Após rodar um teste e o Allure gerar o resultado, para visualiza-lo, executar o seguinte comando:
```
allure serve reports_allure
```
