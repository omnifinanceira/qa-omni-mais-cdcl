require_relative 'Util.rb'

class Login < Util

    set_url '/login'

    element :elementinputusuario, :xpath, "//input[@id='user-login']"
    element :elementinputsenha, :xpath, "//input[@id='user-password']"
    element :elementspanentrar, :xpath, "//a/span[text()='Entrar']"
    element :elementbuttonconfirmar, :xpath, "//div/button[text()='Confirmar']"

    
    def preencher_usuario(usuario)
        elementinputusuario.set usuario
    end

    def preencher_senha(senha)
        elementinputsenha.set senha
    end

    def confirmar_acesso
        elementbuttonconfirmar.click
    end

    def acessar_conta
        elementspanentrar.click
    end

    def acessar_conta_omnimais_loja(usuario = $usuarioloja, senha = $senhalojista)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end

    def acessar_conta_omnimais_premium(usuario = $usuariopremium, senha = $senhapremium)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end
    
    def acessar_conta_omnimais_cessao(usuario = $usuariocessao, senha = $senhacessao)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end

    def acessar_conta_omnimais_cessao_digital(usuario = $usuariodigital, senha = $senhadigital)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end

    def acessar_conta_omnimais_dell(usuario = $usuariodell, senha = $senhadell)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end
end