require_relative 'Util.rb'

class Email < Util
    set_url 'https://mail.tm/en/'

    def pegar_email
        sleep 20
        email = find(:id, 'address', wait: 30).value
        return email
    end
end