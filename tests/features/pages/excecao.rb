require_relative 'GeradorRandomico'
require_relative 'OmniMais'

#validação excecao loja
class Excecao < OmniMais 
    set_url 'https://hmg-omnimaisweb.omni.com.br/login'

    element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"
    element :elementanalisar, :xpath, "//a[text()=' Analisar ']"
    element :elementsalvar, :xpath, "//button[text()='Salvar']"

    def preencher_cpf (cpf)
        find("input[formcontrolname='cpf']").set(cpf)
          $cpf_teste = cpf
    end

    def preencher_cpf_invalido (cpf)
        find("input[formcontrolname='cpf']").set(cpf)
          $cpf_teste = cpf
          find("input[formcontrolname='dataNascimento']").click
    end
  
    def sem_info (cpf)
        find("input[formcontrolname='cpf']").set(cpf)
          $cpf_teste = cpf
          find("input[formcontrolname='dataNascimento']").click
    end  
  
    def preencher_data_nascimento(nascimento)
        find("input[formcontrolname='dataNascimento']").set(nascimento)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_classe_profissional(classeProfissional)
        find("select[name='classeProfissional']").set(classeProfissional)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_valor_renda(renda)
        find("input[name='renda']").set(renda)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_valor_solicitado(valorSolicitado)
        find("input[name='valorSolicitado']").set(valorSolicitado)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_valor_menor(menorValor)
        find("input[name='valorSolicitado']").set(menorValor)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_valor_maior(maiorValor)
        find("input[name='valorSolicitado']").set(maiorValor)
        find("input[formcontrolname='cpf']").click
    end
  
    def preencher_menor_valor_renda(rendaMenor)
        find("input[name='renda']").set(rendaMenor)
        find("input[formcontrolname='cpf']").click
    end
  
    def nao_preencher_campo(tabela)
        find("select[name='']").set(tabela)
        find("input[formcontrolname='cpf']").click
    end
  
    def cep_invalido(cepInvalido)
        find("input[formcontrolname='cep']").set(cepInvalido)
    end

    def cep_obrigatorio(cepObrigatorio)
        find("input[formcontrolname='cep']").set(cepObrigatorio)
        find("input[formcontrolname='cpf']").click
    end

    def endereco_obrigatorio
        find("input[name='logradouro']").click
        find("input[formcontrolname='cpf']").click
    end

    def numero_obrigatorio(numeroEndereco)
        find("input[name='numero']").set(numeroEndereco)
        find("input[formcontrolname='cpf']").click
    end

    def bairro(nomeBairro)
        find("input[name='bairro']").set(nomeBairro)
        find("input[formcontrolname='cpf']").click
    end

    def valor_alterado(valorAlterado)
        first("button[class='btn primary-color']").click
        find("input[formcontrolname='valorFinanciamento']").set(valorAlterado)
    end

    def nao_preencho_parcelamento
        find("i[class='fa fa-plus content-footer__icon']").click
        find("select[name='quantidadeParcelas']").click
        find("h3[class='modal-title']").click
    end

    def nao_preencher_nome_cliente_loja
        find("[formcontrolname='nomeCliente']").native.clear
        find("[formcontrolname='nomeCliente']").set("")
        find("input[formcontrolname='emailCliente']").click
    end

    def preencher_email_invalido(emailInvalido)
        find("input[formcontrolname='emailCliente']").set(emailInvalido)
        find("input[formcontrolname='telefoneCelularCliente']").click
    end

    def nao_preencher_telefone
        find("input[name='telefoneCelularCliente']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='emailCliente']").click
    end
#----------------------------------------------------------------------------#
    def nao_selecionar_genero(genero)
        find('select[formcontrolname="sexoCliente"]').set(genero)
        find('select[formcontrolname="estadoCivilCliente"]').click
    end
   

    def nao_selecionar_estado_civil(estadoCivil)
        find('select[formcontrolname="estadoCivilCliente"]').set(estadoCivil)
        find('select[formcontrolname="sexoCliente"]').click
    end
    

    def nao_preencher_profissao(profissao)
        find('select[formcontrolname="profissaoCliente"]').set(profissao)
        find('select[formcontrolname="sexoCliente"]').click
    end
    
#----------------------------------------------------------------------------#
    def nao_preencho_empresa_trabalho
        find("input[formcontrolname='empresaCliente']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='telefoneEmpresaCliente']").click
    end

    def nao_preencho_telefone_empresa
        find("input[formcontrolname='telefoneEmpresaCliente']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='empresaCliente']").click
    end

    def nao_preencho_telefone_adicional
        find("input[formcontrolname='telefone']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='empresaCliente']").click
    end

    def nao_preencho_nome_referencia
        find("input[formcontrolname='nomeReferencia']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='telefoneReferencia']").click
    end

    def nao_preencho_telefone_referencia
        find("input[formcontrolname='telefoneReferencia']").send_keys :shift, :left, :home, :delete
        find("input[formcontrolname='nomeReferencia']").click
    end

    #verificar como vai rodar a bateria de testes cpf nao pode ter base na omni
    def nao_preencho_cep_comercial(cepComercial)
        find("input[formcontrolname='cep']", " ")
        find("input[formcontrolname='logradouro']").click
        
    end

    def preencher_info_cliente_loja(cpf, nascimento, tabela)
        n = '66'
        cep = '06184140'
        $cpf_teste = cpf
        find("input[formcontrolname='cpf']").set(cpf)
        find("input[formcontrolname='dataNascimento']").set(nascimento)
        fechar_alerta_ficha_existente_loja
        select('SOCIO, PROPRIETARIO, EMPRESARIO', from: 'classeProfissional')
        find("input[formcontrolname='renda']").set(gerar_numero(4000, 8000))
        find("input[formcontrolname='valorSolicitado']").set(gerar_numero(1000, 1000))
        select("#{tabela}", from: 'tabela')
        find("input[formcontrolname='cep']").set(cep)
        find("input[name='numero']").set(n)
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-dados-analise/app-botao-navegacao/div/div/div[2]/a").click
    end
   

    def preencher_detalhes(telefone, nome, sexo)
        @ddd = '55' + telefone.split('')[0..1].join('')
        @telefone = telefone.split('')[2..10].join('')

        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find('input[formcontrolname="nomeCliente"]').set(gerar_nome)
        find('input[name="emailCliente"]').set(gerar_email)
        find('input[formcontrolname="telefoneCelularCliente"]').set(telefone)
        select("#{sexo}", from: 'sexoCliente')
        select('SOLTEIRO', from: 'estadoCivilCliente')
        select('AUTONOMO', from: 'profissaoCliente')
        limpar_e_escrever("//input[@formcontrolname='empresaCliente']", 'AMAZON')
        limpar_e_escrever("//input[@formcontrolname='telefoneEmpresaCliente']", '11331112222')
        limpar_e_escrever("//input[@formcontrolname='telefone']", '11998765432')
        limpar_e_escrever("//input[@formcontrolname='nomeReferencia']", 'Zé')
        limpar_e_escrever("//input[@formcontrolname='telefoneReferencia']", '11111111122')
        
    end

    def continuar_sem_validar_sms
        find(:xpath, "/html/body/modal-container/div/div/app-confirm-modal/div[3]/button[2]").click
    end

    def adicionar_produto
        find("button[class='btn primary-color']", wait: 60).click
        find("select[formcontrolname='tiposMercadoria']", "CELULAR")
        find("input[name='valorProduto']", wait: 60).set(valorProduto)
    end

    def mostrar_fichas_analise
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find("a[href='#aprovadas']").click
        find('a[href="analise"]', wait: 10).click
    end

    def mostrar_fichas_aprovadas
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find("a[href='#aprovadas']").click
    end

    def mostrar_recusadas
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find('a[href="#recusadas"]').click
    end

    def mostrar_emPreenchimento
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        #first("a[title='Ver ficha detalhada']", wait: 120).click
        find('//*[@id="analise"]/app-ficha-em-analise/ol/li[11]/app-ficha-card/div/div[1]/div/a').click
    end

    def carregar_pagina
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        first("a[class='filters']").click
    end

    def mostrar_fichas_canceladas
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find("a[href='#canceladas']").click
    end

    def mostrar_fichas_cdc_loja
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find(:xpath, '//*[@id="records"]/div/div[2]/app-select-produto/div/label/span').click
    end

    def mostrar_todos
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find(:xpath, '//*[@id="records"]/div/div[2]/app-select-produto/div/label/span').click
        first('button', text: "#{$Todos}").click
        
    end

    def mostrar_campo_para_preencher
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find(:xpath, '//*[@id="records"]/div/ul/li[6]/a').click
    end

    def preencher_Nome_loja
        find('input[name="nome"]').set(nomeLoja)
    end

    def nao_preencher_info_comercial(enderecoComercial, numeroComercial, bairro)
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-enderecos-cliente/form/div/div/div/app-endereco-base[2]/form/div[1]/div/input").click
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-enderecos-cliente/form/div/div/div/app-endereco-base[2]/form/div[3]/div/div[1]/input").click
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-enderecos-cliente/form/div/div/div/app-endereco-base[2]/form/div[3]/div/div[2]/input").click
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-enderecos-cliente/form/div/div/div/app-endereco-base[2]/form/div[4]/div/input").click
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-enderecos-cliente/form/div/div/div/app-endereco-base[2]/form/div[3]/div/div[1]/input").click
        
    end

    def valor_mercadoria_teste(valor)
        @valor_mercadoria_teste = valor.to_s + "000"
        return @valor_mercadoria_teste
    end

    def preencher_demais_dados_cliente_teste 
        page.attach_file("#{Dir.pwd}\\imagens\\foto_pessoa.jpg") do
            page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").visible?
            page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").click
            end
          end
        
          def adicionar_produto_teste
            find("button[class='btn primary-color']").click  
             valor_mercadoria_teste (10000)
             select('ELETRÔNICOS', from: 'tiposMercadoria')
             find("input[formcontrolname='valorProduto']").set(@valor_mercadoria_teste)
          end

    #Necessário alterar valor_mercadoria_teste para testar menor e maior valor alterado.      
    def adicionar_protudo_teste
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-identificacao/form/div/div/app-panel-custom[2]/div/div[2]/div/div[1]/app-btn-small/button").click
            valor_mercadoria_teste (90)
            select('ELETRÔNICOS', from: 'tiposMercadoria')
            find("input[formcontrolname='valorProduto']").set(@valor_mercadoria_teste)
            find("button[class='btn primary-color']").click
            valor_mercadoria_teste (90)
            sleep 2
            find(:xpath, "//app-mercadoria[2]//select").find(:xpath, "//app-mercadoria[2]//select//option[text()=' FERRAMENTAS ']").select_option
            find(:xpath, "//app-mercadoria[2]//input").set(@valor_mercadoria_teste)
    end

    def selecionar_operacao_t(operacao)
        select("#{operacao}", from: 'codigosOperacoes', wait: 20)
    end

    def selecionar_vendedor_loja_t
        sleep 5
        find("span[class='chevron fa fa-chevron-down']").click
        find(:xpath, "//app-auto-complete[@formcontrolname='vendedorId']/ul/li/a[text()='CARLOS']").click 
    end

    def preencher_dolar(cpf, nascimento, classeProfissional)
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find("input[formcontrolname='cpf']").set(cpf)
        sleep 3
        find('input[formcontrolname="dataNascimento"]').set(nascimento)
        sleep 3
        fechar_alerta_ficha_existente_loja
        find(:xpath, "/html/body/app-root/main/ng-component/main/section/app-dados-analise/div/form[1]/div/div[3]/div/select/option[6]").click
        sleep 3
    end

    def preencher_conjuge(cpf, nascimento)
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find("input[formcontrolname='cpfConjuge']").set(cpf)
        find("input[formcontrolname='dataNascimentoConjuge']").set(nascimento)
    end

    def preencher_infoConjuge(cpf_conjuge, nascimento, tabela)
        n = '66'
        cep = '06184140'
        find("input[formcontrolname='cpfConjuge']").set(cpf_conjuge)
        find("input[formcontrolname='dataNascimentoConjuge']").set(nascimento)
        fechar_alerta_ficha_existente_loja
        find("input[formcontrolname='renda']").set(gerar_numero(4000, 8000))
        find("input[formcontrolname='valorSolicitado']").set(gerar_numero(1000, 1000))
        select("#{tabela}", from: 'tabela')
        find("input[formcontrolname='cep']").set(cep)
        find("input[name='numero']").set(n)
    end

    def selecionar_data_vencimento
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        find('i[class="fa fa-calendar"]').click
        first('td[class="day"]').click
        find('button[class="btn btn-full-orange pull-right"]').click
    end



    #----------------------leo daqui pra baixo------------------------------#

    def acessar_aba_recusada
        find("a[href='#recusadas']", wait: 120).click
    end

    def acessar_aba_analise
        find("a[href='#analise']", wait: 120).click
    end

    def acessar_aba_aprovada
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        binding.pry
        find("a[href='#aprovadas']", wait: 120).click
        binding.pry
    end

    def filtrar_fichas
        find("a[title='Filtrar fichas de análise']", wait: 60).click
    end

    def pesquisar_ficha_cpf(cpf)
        find("input[name='nome']", wait: 60).set(cpf)
    end

    def pesquisar_ficha
        find("input[name='numero']", wait: 60).set($proposta)
    end

    def preencher_dados_cliente_loja_exec(cpf, nascimento, tabela)
        n = '66'
        cep = '19020-380'
        $cpf_teste = cpf
        find("input[formcontrolname='cpf']").set(cpf)
        find("input[formcontrolname='dataNascimento']").set(nascimento)
        fechar_alerta_ficha_existente_loja
        select('AUTONOMO', from: 'classeProfissional')
        find("input[formcontrolname='renda']").set(gerar_numero(4000, 8000))
        find("input[formcontrolname='valorSolicitado']").set(gerar_numero(400, 4000))
        select("#{tabela}", from: 'tabela')
        limpar_e_escrever("//input[@formcontrolname='cep']", cep)
        find("input[name='numero']").set(n)
      end

    def nao_selecionar_endereco_correspondencia
        select('RESIDENCIAL', from: 'enderecoCorrespondencia', wait: 60)
    end

    def nao_aprovacao_proposta
        within_window $nova_janela do
        #fechar_pop_up
          within_frame(find(:xpath, '//iframe')[:id]) do
            within_frame(all(:xpath, '//frameset')[0].all(:xpath, '//frame')[1][:name]) do
              find("input[id='buttonCollapse']", wait: 60).click
              find("input[value='Recusar']", wait: 60).click
              accept_confirm('Deseja finalizar a proposta com status RECUSADA?')
            end
          end
        end
    end
    
    def motivo_recusa
        within_window $nova_janela do
            within_frame(find(:xpath, '//iframe')[:id]) do
              within_frame(all(:xpath, '//frameset')[0].all(:xpath, '//frame')[1][:name]) do
                select("RENDA NÃO COMPROVADA", from: 'p_motivo_recusa1')
                find("textarea[name='p_parecer']").set("RECUSA PROPOSTA TESTE QA VERI")
                find("input[value='Validar']").click
                accept_confirm('Deseja finalizar a proposta?')
              end
            end
        end
    end

    def valida_ficha_numero
        texto_ficha = find('span', text: "#{$proposta}", wait: 60).text
        return texto_ficha
    end

    def verificar_filtro_x_pagina
       texto_numero = find("span[class='user-id']", match: :first, wait: 60).text.gsub(/[^0-9]/, '')
       return texto_numero
    end

    def verificar_situacao_ficha(situacao)
        texto = page.has_content?(teste, wait: 60)
        return texto
    end

    def verificar_excecao(mensagem)
        texto = page.has_text?(mensagem, minimum: 1, wait: 60)
        
        return texto
    end
end

