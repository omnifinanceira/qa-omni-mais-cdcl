class Home < SitePrism::Page

    set_url ''

    element :elementameudesempenho, :xpath, "//a[@href='/meu-desempenho']"
    element :elementaopcoesdeusuario, :xpath, "//a[@title='Opções do usuário']"
    element :elementaalteraragente, :xpath, "//a[text()=' Alterar Agente']"
    element :elementaconfiguracoes, :xpath, "//a[@routerlink='/configuracoes']"
    element :elementasair, :xpath, "//a[@href='/login']"
    element :elementranking, :xpath, "//a[@title='Ranking']"
    element :elementficha, :xpath, "//a[@title='Adicionar nova ficha']"

    def acessar_meu_desempenho
        elementameudesempenho.click
    end

    def acessar_opcoes_de_usuario
        elementaopcoesdeusuario.click
    end

    def acessar_alterar_agente
        elementaalteraragente.click
    end

    def acessar_configuracoes
        elementaconfiguracoes.click
    end

    def sair
        elementasair.click
    end

    def acessar_ranking
        elementranking.click
    end

    def acessar_ficha
        find("a[title='Adicionar nova ficha']", wait: 120).click
    end
end