require 'pry'

class OmniFacil < SitePrism::Page
  element :mesa_de_credito, :xpath, "//a[@id='sel-sistema[11]']"
  element :habilitar_lista_agente, :xpath, "//span[@id='select2-p-agente-container']"
  element :selecionar_agente, :xpath, "//li[text()='1631 - CDCL SNV SUL - CURITIBA-PR']"
  element :selecionar_agente_cessao, :xpath, "//li[text()='2872 - CANAL DIRETO - CDC LOJA - COTIA-SP']"
  element :validar, :xpath, "//button[@id='bt-validar']"

  def acessar_mesa_credito
    mesa_de_credito.click
  end

  def acessar_novo_menu
    find(:xpath, "//a[@id='sel-sistema[1]']").click
  end

  def acessar_menu_snv
    find(:id, 'home-omni').click
    find("a[id='sel-sistema[6]']").click
  end

  def listar_agente
    habilitar_lista_agente.click
    selecionar_agente.click
    validar.click
  end

  def listar_agente_cessao
    habilitar_lista_agente.click
    selecionar_agente_cessao.click
    validar.click
  end

  def pesquisar_numero_proposta(numero_proposta)
    find("input[name='paramProposta-inputEl']").set(numero_proposta)
    find("span[id='buttonPesquisar-btnIconEl']").click
    find(:xpath, "//*[@id='gridview-1029-hd-CDCL SUL']/div", wait: 60).click

    @nova_janela = window_opened_by do
      find(:xpath, "//a[text()='#{numero_proposta}']").click
    end
  end

  def pesquisar_numero_proposta_cessao(numero_proposta)
    puts numero_proposta
    find("input[name='paramProposta-inputEl']").set(numero_proposta)
    find("span[id='buttonPesquisar-btnIconEl']").click
    find(:xpath, "//div[@id='gridview-1029-hd-CDC LOJA CANAL DIRETO']", wait: 60).click

    @nova_janela = window_opened_by do
      find(:xpath, "//a[text()='#{numero_proposta}']", wait: 60 ).click
    end
  end


  def fechar_pop_up
    within_frame(find(:xpath, '//iframe')[:id]) do
      within_frame(all(:xpath, '//frameset')[0].all(:xpath, '//frame')[1][:name]) do
        find(:xpath, "//div/input[@id='popup_ok']").click if find(:xpath, "//div[@id='popup_content']").visible?
      end
    end
  rescue Exception => e
    e.message
  end

  def aprovacao_proposta
    within_window @nova_janela do
      fechar_pop_up
      within_frame(find(:xpath, '//iframe', wait: 60)[:id]) do
        within_frame(all(:xpath, '//frameset')[0].all(:xpath, '//frame')[1][:name]) do
          find("input[id='buttonCollapse']", wait: 60).click
          find("input[value='Aprovar']", wait: 60).click
          accept_confirm('Deseja finalizar a proposta com status APROVADA?')
        end
      end
    end
  end

  def preencher_check_list
    within_window @nova_janela do
      within_frame(find(:xpath, '//iframe', wait: 60)[:id]) do
        within_frame(all(:xpath, '//frameset')[0].all(:xpath, '//frame')[1][:name]) do
          find("input[value='#008_COMPROVANTE DE RENDA']", wait: 60).click
          find("textarea[name='p_parecer']", wait:60).set('Teste QA CDCL')
          find("input[value='Validar']", wait: 60).click
          accept_confirm('Deseja finalizar a proposta?')
        end
      end
    end
  end

  def selecionar_menu(menu = nil)
    find(:xpath, "//li/a[text()=' #{menu}']").click unless menu.nil?
  end
  
  def selecionar_submenu(submenu = nil)
    find(:xpath, "//ul/li/a[text()='#{submenu}']").click unless submenu.nil?
  end

  def pesquisar_contrato
    within_frame(find(:xpath, '//iframe')[:id]) do
      within_frame(find(:xpath, '//frameset').all(:xpath, '//frame')[0][:name]) do
        find("input[name='p_contrato']").set($contrato)
        puts $contrato
        puts $proposta
        find("input[value='Pesquisar']").click
      end
    end
  end

  def pesquisar_contrato_snv
    within_frame(find(:xpath, '//iframe').all(:xpath, '//iframe')[0][:name]) do
      find("input[name='P_CONTRATO_SNV']").set($contrato)
      find("input[value='Pesquisar']").click
    end
  end
  
  def acessar_contrato_snv
    within_frame(find(:xpath, '//iframe').all(:xpath, '//iframe')[0][:name]) do
    @formalizacao_snv = window_opened_by { find(:xpath, "//a[contains(text(), '#{$contrato}')]").click }
    end
  end 

  def formalizar_contrato_snv
    within_window @formalizacao_snv do
      within_frame(find(:xpath, '//frameset').all(:xpath, '//frame')[0][:name]) do
        accept_alert do
          accept_alert do
            find("input[value='Formalizar']").click
          end
        end
        page.driver.browser.switch_to.window(page.driver.browser.window_handles.last)
      end
    end
  end

  def botao_formalizar
    within_frame(find(:xpath, '//iframe')[:id]) do
      within_frame(find(:xpath, '//frameset').all(:xpath, '//frame')[1][:name]) do
        @formalizacao = window_opened_by { click_button 'Formalizar' }
      end
    end
  end

  def aceite_documentos
    within_window @formalizacao do
    find(:id, "conf").click
    find(:id, "popup_ok").click
    find("input[value='Fechar']").click
    end
  end

  def pesquisa_proposta_para_verificar_imagem
    find("input[id='p_indice_valor_1']", wait: 60 ).set($proposta)
    sleep 2
    click_button('btnPesquisar', wait: 60 )
    sleep 2
    texto = find('td', text: 'Foto do Cliente', wait: 60 ).text
    texto_formatado = texto.gsub(/[[:space:]]/, '').downcase
    return texto_formatado
  end

end