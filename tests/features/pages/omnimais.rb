require_relative 'geradorrandomico'
require 'pry'

class OmniMais < GeradorRandomico
  set_url ''

  element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"
  element :elementanalisar, :xpath, "//a[text()=' Analisar ']"
  element :elementsalvar, :xpath, "//button[text()='Salvar']"
  element :simular, :xpath, "//a[text()=' Simular ']"
  #element :continuar_dell, :xpath, "//a[text()=' Continuar ']"
  #element :enviar_proposta_dell, :xpath, "//a[text()=' Enviar proposta ']"
  element :analisar_dell, :xpath, "//i[@class='fa fa-arrow-right mlBt']"

  def initialize
    @sql = Queries.new
  end

  def clicar_em_simular
    simular.click
  end

  def clicar_em_continuar_dell
    find('a', text: 'Continuar', wait: 60).click
  end

  def clicar_em_enviar_proposta_dell
    find('a', text: 'Enviar proposta', wait: 60).click
  end
  
  def clicar_em_analisar_dell
    find('a', text: 'Analisar', wait: 60).click
  end

  def selecionar_opcao(opcao)
    find(:xpath, "//app-button-selector/div/div[text()='#{opcao}']", wait: 10).click
  end

  def selecionar_operacao(operacao)
    select("#{operacao}", from: 'codigosOperacoes', wait: 20)
  end

  def selecionar_operacao_cessao(operacao)
    select("#{operacao}", from: 'codigosOperacoes', wait: 60)
  end

  def clicar_em_continuar
    find("i[class='fa fa-arrow-right mlBt']", wait: 60).click
  end

  def clicar_em_continuar_entrada
    find("span[class='fa fa-arrow-right mlBt']", wait: 60).click
  end

  def clicar_em_analisar
    elementanalisar.click
  end

  def clicar_em_salvar
    elementsalvar.click
  end

  def selecionar_vendedor_loja
    sleep(5)
    find("span[class='chevron fa fa-chevron-down']", wait: 60).click
    find(:xpath, "//app-auto-complete[@formcontrolname='vendedorId']/ul/li/a[text()='Não cadastrado']").click
  end

  def pegar_horario
    horarioagora = Time.new
    horarioagora.strftime('%H:%M')
  end

  def fechar_alerta_ficha_existente_loja
    if find('h3', exact_text: 'Ficha em andamento encontrada!').visible? 
      click_button("Iniciar nova ficha")
    end
  rescue Exception => e
    e.message
  end

  def preencher_dados_cliente_loja(cpf, nascimento, tabela)
    n = '66'
    cep = '06184140'
    $cpf_teste = cpf
    find("input[formcontrolname='cpf']").set(cpf)
    find("input[formcontrolname='dataNascimento']").set(nascimento)
    fechar_alerta_ficha_existente_loja
    select('AUTONOMO', from: 'classeProfissional')
    find("input[formcontrolname='renda']").set(gerar_numero(4000, 8000))
    find("input[formcontrolname='valorSolicitado']").set(gerar_numero(400, 4000))
    select("#{tabela}", from: 'tabela', wait: 120)
    find("input[formcontrolname='cep']").set(cep)
    find("input[name='numero']").set(n)
  end

  def preencher_dados_cliente_premium(cpf, nascimento, tabela)
    n = '66'
    cep = '06184140'
    $cpf_teste = cpf
    find("input[formcontrolname='cpf']").set(cpf)
    find("input[formcontrolname='dataNascimento']").set(nascimento)
    fechar_alerta_ficha_existente_loja
    select('AUTONOMO', from: 'classeProfissional')
    find("input[formcontrolname='renda']").set(gerar_numero(4000, 8000))
    find("input[formcontrolname='valorSolicitado']").set(gerar_numero(1000, 1000))
    select("#{tabela}", from: 'tabela')
    find("input[formcontrolname='cep']").set(cep)
    find("input[name='numero']").set(n)
  end

  def selecionar_parcelas_loja(qtd_parcela)
    $qtd_parcela = qtd_parcela
    $wait.until { find("label[for='#{$qtd_parcela}']").select_option }
  end

  def selecionar_seguro(seguro)
    find("span[class='fa fa-gear']", wait: 60).click
    find("label[for='radioAssistencia0']").click
    find(:xpath, "//p[text()='#{seguro}']").click
    find('a', text: 'Salvar').click
  end

  def selecionar_assistencia(assistencia)
    find("span[class='fa fa-gear']", wait: 60).click
    find("label[for='radioSeguro0']").click
    find(:xpath, "//p[text()='#{assistencia}']").click
    find('a', text: 'Salvar').click
  end

  def selecionar_seguro_assistencia(seguro, assistencia)
    find("span[class='fa fa-gear']", wait: 60).click
    find(:xpath, "//p[text()='#{assistencia}']").click
    find(:xpath, "//p[text()='#{seguro}']").click
    find('a', text: 'Salvar').click
  end

  def selecionar_nenhum
    find("span[class='fa fa-gear']", wait: 60).click
    find("label[for='radioSeguro0']").click
    find("label[for='radioAssistencia0']").click
    find('a', text: 'Salvar').click
  end

  def armazenar_dados_proposta
    parcelas_valor_auxiliar = find(:xpath, "//label[@for='#{$qtd_parcela}']/span").text.split("\n")
    $quantidade_parcela_omni_mais = parcelas_valor_auxiliar[0].gsub('x', '')
    $valor_parcela_omni_mais = parcelas_valor_auxiliar[1].gsub('R$ ', '')
  end

  
  def analise_assincrona
    aguardar_elemento_ficar_invisivel_assincrono("//div[@class='loader-phases']") 
    sleep 10
    botao_analisar = page.has_xpath?("//div[@class='btn-analisar-novamente']")
    if botao_analisar == true
      find(:xpath, "//button[text()=' Analisar novamente ']", wait: 60).click
      aguardar_elemento_ficar_invisivel_assincrono("//div[@class='loader-phases']") 
      botao_analisar_tentativa2 = page.has_xpath?("//div[@class='btn-analisar-novamente']")
      return botao_analisar_tentativa2
    end
  end

  def preencher_detalhes_cliente_loja(telefone, nome, sexo)
    @ddd = '55' + telefone.split('')[0..1].join('')
    @telefone = telefone.split('')[2..10].join('')

    aguardar_elemento_ficar_invisivel("//div[@id='loader']")
    limpar_e_escrever("//input[@formcontrolname='nomeCliente']", nome)
    limpar_e_escrever("//input[@formcontrolname='emailCliente']", gerar_email)
    limpar_e_escrever("//input[@formcontrolname='telefoneCelularCliente']", telefone)
    select("#{sexo}", from: 'sexoCliente')
    select('SOLTEIRO', from: 'estadoCivilCliente')
    select('AUTONOMO', from: 'profissaoCliente')
    limpar_e_escrever("//input[@formcontrolname='empresaCliente']", 'OMNI')
    limpar_e_escrever("//input[@formcontrolname='telefoneEmpresaCliente']", '1133653500')
    limpar_e_escrever("//input[@formcontrolname='telefone']", '11998765432')
    limpar_e_escrever("//input[@formcontrolname='nomeReferencia']", 'JULIANA AMIGA')
    limpar_e_escrever("//input[@formcontrolname='telefoneReferencia']", '11997543627')
  end

  def validar_sms
    find('a', text: 'Enviar Token para o celular').click
    sleep(10)
    @token = ''
    aguardar_elemento_ficar_visivel("//input[@formcontrolname='codigoConfirmacao']")
    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token
    end
    find("input[formcontrolname='codigoConfirmacao']").set(@token)
    sleep(5)
    aguardar_elemento_ficar_visivel("//div[text()=' Token Validado com Sucesso ']")
  end

  def selecionar_endereco_correspondencia
    select('RESIDENCIAL', from: 'enderecoCorrespondencia', wait: 60)
  end

  def valor_mercadoria
    pegarvalor = all(:xpath, "//div[@class='form-group show-total']/div/p")[1].text
    @valormercadoria = pegarvalor.gsub(/[^0-9]/, '')
    @valormercadoria
  end

  def valor_mercadoria_p(valor)
    @valor_mercadoria = valor.to_s + "00"
    return @valor_mercadoria
  end  

  def anexar_foto
    page.attach_file("#{Dir.pwd}\\imagens\\foto_pessoa.jpg") do
    aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").visible?
    page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").click
    end
  end

  def preencher_demais_dados_cliente_loja
    find('span', text: 'Adicionar produto').click
    valor_mercadoria
    select('ELETRÔNICOS', from: 'tiposMercadoria')
    find("input[formcontrolname='valorProduto']").set(@valormercadoria)
  end

  def preencher_demais_dados_cliente_loja_biometria #teste assistido
    binding.pry
    sleep 5
    find('span', text: 'Adicionar produto').click
    valor_mercadoria
    select('ELETRÔNICOS', from: 'tiposMercadoria')
    find("input[formcontrolname='valorProduto']").set(@valormercadoria)
  end

  def preencher_demais_dados_cliente_premium
    page.attach_file("#{Dir.pwd}\\imagens\\foto_pessoa.jpg") do
    page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").visible?
    page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").click
    end
  end

  def adicionar_produto
    find("button[class='btn primary-color']").click   
     valor_mercadoria_p (1000)
     select('ELETRÔNICOS', from: 'tiposMercadoria')
     find("input[formcontrolname='valorProduto']").set(@valor_mercadoria)
  end

  # soma das mercadorias tem que bater com o valor solicitado de emprestimo
  def adicionar_mais_produto
    find("button[class='btn primary-color']").click   
      valor_mercadoria_p (200)
      select('ELETRÔNICOS', from: 'tiposMercadoria')
      find("input[formcontrolname='valorProduto']").set(@valor_mercadoria)
      find("button[class='btn primary-color']").click

      valor_mercadoria_p (200)
      sleep 2
      find(:xpath, "//app-mercadoria[2]//select").find(:xpath, "//app-mercadoria[2]//select//option[text()=' FERRAMENTAS ']").select_option
      find(:xpath, "//app-mercadoria[2]//input").set(@valor_mercadoria)
  end

  def aguardar_elemento_ficar_invisivel(xpath)
    @horaatual = pegar_horario
    contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
    begin
      @horaatual = pegar_horario while find(:xpath, xpath.to_s).visible? && contagem != @horaatual
      result = (contagem != @horaatual)
    rescue StandardError
    end
    result
  end

  def aguardar_elemento_ficar_invisivel_assincrono(xpath)
    @horaatual = pegar_horario
    contagem = (Time.parse(@horaatual) + 240).strftime('%H:%M')
    begin
      @horaatual = pegar_horario while find(:xpath, xpath.to_s).visible? && contagem != @horaatual
      result = (contagem != @horaatual)
    rescue StandardError
    end
    result
  end

  def aguardar_elemento_ficar_visivel(xpath)
    @horaatual = pegar_horario
    contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
    begin
      @horaatual = pegar_horario while !find(:xpath, xpath.to_s).visible? && contagem != @horaatual
      result = (contagem != @horaatual)
    rescue StandardError
    end
    result
  end

  def acessar_aba_fichas_aprovadas
    find("a[href='#aprovadas']", wait: 60).click
  end

  def acessar_aba_pagamentos
    find("a[href='#fechamento']").click
  end

  def acessar_aba_fichas_pagamentos
    find(:xpath, "//button[contains(text(), 'Voltar')]", wait: 20).click
    find('a', text: 'Minhas Fichas').click
    find("a[href='#fechamento']").click
  end

  def pesquisar_e_acessar_proposta
    find("a[title='Filtrar fichas de análise']").click
    find("input[name='numero']").set($proposta)
    sleep(10)
    find("a[title='Ver ficha detalhada']", wait: 120).click
  end

  def pesquisar_proposta_valida
    find("a[title='Filtrar fichas de análise']", wait: 60).click
    find("input[name='numero']", wait: 60).set($proposta)
    sleep(10)
  end

  def fechar_negocio
    find('button', text: 'Sim', wait: 20).click
  end

  def preencher_dados_complementares
    select('Registro Geral - RG', from:'user-id')
    find("input[name='numeroDocumentoCliente']").set('472551844')
    find("input[name='orgaoDocumentoCliente']").set('ssp')
    find("input[name='emissaoDocumentoCliente']").set('01052015')
    find("input[name='nomeMaeClienteText']").set('Maria da Silva')
    find("input[name='ufn']").check
    select('BRASILEIRA', from:'nacionalidadeCliente', wait: 60)
    select('SP - São Paulo', from:'naturalUfCliente')
    select('SAO PAULO', from:'naturalCidadeCliente')
    find("input[id='user-patrimony']").set('10000' + '00')
    clicar_em_salvar
  end

  def anexar_documentos
    sleep(5)
    find('button', text: 'Anexar Documentos', wait: 60).click
    sleep(5)
    page.attach_file("#{Dir.pwd}\\imagens\\rg.png") do
      page.find(:xpath, "//div[text()=' RG ']").click
    end
    page.attach_file("#{Dir.pwd}\\imagens\\cpf.pdf") do
      page.find(:xpath, "//div[text()=' CPF ']").click
    end
    sleep(10)
    find('button', text: 'Confirmar seleção').click
    sleep(5)
    find('button', text: 'Enviar Documentos para Formalização').click
  end

  # todos os tokens
  def assinatura_digital
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar Token']").click
    sleep(5)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(15)
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token
      puts @token
      @tokenccb = @token.slice(0, 6)
      @tokenseguro = @token.slice(6, @token.size - 12)
      @tokenassistencia = @token.slice(12, @token.size)
      puts @tokenccb
      puts @tokenseguro
      puts @tokenassistencia
    end
    find('a', text: 'Abrir Contratos').click
    click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
    click_button('action-bar-btn-continue')
    sleep(10)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[1].set(@tokenccb)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[5].set(@tokenseguro)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[9].set(@tokenassistencia)
    click_button('action-bar-btn-finish')
    find("button[data-qa='dialog-submit']").click
    find('button', text: 'Enviar para formalização', wait: 100).click
    salvar_contrato
  end

  # só o token de ccb e seguro
  def assinatura_digital_seguro
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar Token']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(15)
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token
      puts @token
      @tokenccb = @token.slice(0, 6)
      @tokenseguro = @token.slice(6, 12)
    end
    find('a', text: 'Abrir Contratos').click
    click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
    click_button('action-bar-btn-continue')
    sleep(10)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[1].set(@tokenccb)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[5].set(@tokenseguro)
    click_button('action-bar-btn-finish')
    find("button[data-qa='dialog-submit']").click
    find('button', text: 'Enviar para formalização', wait: 100).click
    salvar_contrato
  end

  # só o token de ccb e assistencia
  def assinatura_digital_assistencia
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar Token']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(15)
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token
      puts @token
      @tokenccb = @token.slice(0, 6)
      @tokenassistencia = @token.slice(6, 12)
    end
    find('a', text: 'Abrir Contratos').click
    click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
    click_button('action-bar-btn-continue')
    sleep(10)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[1].set(@tokenccb)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[5].set(@tokenassistencia)
    click_button('action-bar-btn-finish')
    find("button[data-qa='dialog-submit']").click
    find('button', text: 'Enviar para formalização', wait: 100).click
    salvar_contrato
  end

  #so token ccb
  def assinatura_digital_nenhum
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar Token']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(15)
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token
      puts @token
      @tokenccb = @token.slice(0, 6)
    end
    find('a', text: 'Abrir Contratos').click
    click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
    click_button('action-bar-btn-continue')
    sleep(10)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[0].set(@tokenccb)
    click_button('action-bar-btn-finish')
    find("button[data-qa='dialog-submit']").click
    find('button', text: 'Enviar para formalização', wait: 100).click
    salvar_contrato
    sleep 20
  end

  def assinatura_digital_cessao
    sleep(5)
    find('button', text: 'Assinar Documentos', wait: 60).visible?
    find('button', text: 'Assinar Documentos', wait: 60).click
    find(:xpath, "//label[text()='Enviar Token']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(15)
      @sms.pesquisar_sms(@ddd, @telefone)
      @token = @sms.pegar_token_cadastro_cessao
      @tokencadastro = @token.slice(0, 6)
    end
    find('a', text: 'Abrir Contratos', wait: 60).click
    click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
    click_button('action-bar-btn-continue')
    sleep(5)
    all(:xpath, "//input[contains(@id, 'tab-form-element')]")[0].set(@tokencadastro)
    sleep(5)
    click_button('action-bar-btn-finish')
    find("button[data-qa='dialog-submit']").click
    aguardar_elemento_ficar_invisivel("//div[@id='loader']")
    aguardar_elemento_ficar_invisivel("//div[@id='loader']")
    find('button', text: 'Enviar para formalização', wait: 60).click
    salvar_contrato
    sleep 20
  end

  def assinatura_digital_celular
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar mensagem']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(10)
      @sms.pesquisar_sms(@ddd, @telefone)
      @link = @sms.pegar_link_assinatura
      puts @link
    end

    nova_janela = open_new_window
    within_window nova_janela do
      celular = Celular.new
      celular.load(link: @link)
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end
      salvar_contrato_celular
      sleep 20
      page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
    end
  end

  def assinatura_digital_celular_cessao
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar mensagem']").click
    sleep(5)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      @sms.pesquisar_sms(@ddd, @telefone)
      @link = @sms.pegar_link_assinatura
      puts @link
    end

    nova_janela = open_new_window
    within_window nova_janela do
      celular = Celular.new
      celular.load(link: @link)
      find("input[name='p_cpf']", wait: 60).set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end

      find(:xpath, "//*[@id='in-person-pass-control-modal-content']/div[3]/button").click
      find(:xpath, "//span[text()='Sim']", wait: 120).click
      salvar_contrato_celular
      sleep 20
    end
  end

  def assinatura_digital_celular_dell
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos', wait: 60).click
    find(:xpath, "//label[text()='Enviar mensagem']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(10)
      @sms.pesquisar_sms(@ddd, @telefone)
      @link = @sms.pegar_link_assinatura
      puts @link
    end

    nova_janela = open_new_window
    within_window nova_janela do
      celular = Celular.new
      celular.load(link: @link)
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end
      salvar_contrato_celular
      sleep 20
      page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
    end
  end

  def assinatura_digital_email
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//a[text()='Não é este e-mail? Alterar']").click
    sleep(5)

    @nova_janela = open_new_window
    within_window @nova_janela do
      email = Email.new
      email.load
      @setEmail = email.pegar_email
    end

    find("input[name='email']").set(@setEmail)
    find('button', text: 'Salvar').click
    find(:xpath, "//label[text()='Enviar E-mail']").click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
    find(:xpath, "//div[contains(@id, '__layout')]//ul//img", wait: 320).click

    within_frame(find(:xpath, '//iframe')) do
      @assinatura_email = window_opened_by { find(:xpath, '//html/body/a').click }
    end

    within_window @assinatura_email do
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end
      salvar_contrato_email
      sleep 20
    end
    page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  end

  def assinatura_digital_email_dell
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//a[text()='Não é este e-mail? Alterar']").click
    sleep(5)

    @nova_janela = open_new_window
    within_window @nova_janela do
      email = Email.new
      email.load
      @setEmail = email.pegar_email
    end

    find("input[name='email']").set(@setEmail)
    find('button', text: 'Salvar').click
    find(:xpath, "//label[text()='Enviar E-mail']").click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles[4])
    find(:xpath, "//div[contains(@id, '__layout')]//ul//img", wait: 320).click

    within_frame(find(:xpath, '//iframe')) do
      @assinatura_email = window_opened_by { find(:xpath, '//html/body/a').click }
    end

    within_window @assinatura_email do
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF', wait: 60)
      sleep 5
      click_button('action-bar-btn-continue', wait: 60)

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end
      salvar_contrato_email
      sleep 20
    end
    page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  end

  def assinatura_digital_email_cessao
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//a[text()='Não é este e-mail? Alterar']").click
    sleep(5)

    @nova_janela = open_new_window
    within_window @nova_janela do
      email = Email.new
      email.load
      @setEmail = email.pegar_email
    end

    find("input[name='email']").set(@setEmail)
    find('button', text: 'Salvar').click
    find(:xpath, "//label[text()='Enviar E-mail']").click
    page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
    find(:xpath, "//div[contains(@id, '__layout')]//ul//img", wait: 320).click
    
    within_frame(find(:xpath, '//iframe')) do
      @assinatura_email = window_opened_by { find(:xpath, '//html/body/a').click }
    end

    within_window @assinatura_email do
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('ds_hldrBdy_btnDSInPersonIDControl_btnInline')
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end

      find(:xpath, "//*[@id='in-person-pass-control-modal-content']/div[3]/button").click
      find(:xpath, "//span[text()='Sim']", wait: 120).click
      salvar_contrato_email
      sleep 20
    end
      page.driver.browser.switch_to.window(page.driver.browser.window_handles[0])
  end

  def assinatura_digital_celular_premium
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//label[text()='Enviar mensagem']").click
    sleep(15)

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(10)
      @sms.pesquisar_sms(@ddd, @telefone)
      @link = @sms.pegar_link_assinatura
      puts @link
    end

    nova_janela = open_new_window
    within_window nova_janela do
      celular = Celular.new
      celular.load(link: @link)
      find("input[name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end

      find(:xpath, "//span[text()='Sim']", wait: 120).click
      salvar_contrato_celular_premium
      sleep 20
    end
  end

  def assinatura_digital_email_premium
    sleep(5)
    find('button', text: 'Assinar Documentos').visible?
    find('button', text: 'Assinar Documentos').click
    find(:xpath, "//a[text()='Não é este e-mail? Alterar']").click
    sleep(5)

    @nova_janela = open_new_window
    within_window @nova_janela do
      email = Email.new
      email.load
      @setEmail = email.pegar_email
    end

    find("input[name='email']").set(@setEmail)
    find('button', text: 'Salvar').click
    find(:xpath, "//label[text()='Enviar E-mail']").click

    page.driver.browser.switch_to.window(page.driver.browser.window_handles[3])
    find(:xpath, "//div[contains(@id, '__layout')]//ul//img", wait: 320).click

    within_frame(find(:xpath, '//iframe')) do
      @assinatura_email = window_opened_by { find(:xpath, '//html/body/a').click }
    end

    within_window @assinatura_email do
      find(:xpath, "//input[@name='p_cpf']").set($cpf_teste)
      click_button('btValidarCPF')
      sleep 5
      click_button('action-bar-btn-continue')

      all(:xpath, "//button[contains(@id,'tab-form-element')]//span[2]").each do |clicaBotao|
        sleep 5
        scroll_to(clicaBotao, align: :top)
        page.execute_script('arguments[0].click()', clicaBotao)
      end

      find(:xpath, "//span[text()='Sim']", wait: 120).click
      salvar_contrato_email_premium
      sleep 20
    end
  end

  def enviar_formalizacao
    find(:xpath, "//button[text()='Enviar para formalização']", wait: 20).visible?
    find(:xpath, "//button[text()='Enviar para formalização']", wait: 20).click
    sleep 20
  end

  def salvar_contrato_email
    $contrato = find('h3', wait: 10).text.gsub(/[^0-9]/, '')
  end

  def salvar_contrato_celular
    $contrato = find('h3', wait: 10).text.gsub(/[^0-9]/, '')
  end

  def salvar_contrato_celular_premium
    $contrato = find('h3', wait: 10).text.gsub(/[^0-9]/, '')
  end

  def salvar_contrato_email_premium
    $contrato = find('h3', wait: 10).text.gsub(/[^0-9]/, '')
  end

  def salvar_contrato
    $contrato = find(:xpath, "//div[contains(text(), 'Contrato')]", wait: 60).text.gsub(/[^0-9]/, '')
  end

  def pesquisar_proposta_digital(numero_proposta)
    puts numero_proposta
    aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    sleep 5
    find("span[class='fa fa-search']", wait: 60).click
    find("input[id='filtroProposta']", wait: 60).set(numero_proposta)
    sleep 2
    find("button[type='submit']", wait: 60).click
    sleep 2
    find("button[class='icon cessao-documento-assinar lined']", wait: 60).click
  end

  def selecionar_carencia(value)
    find("button[value='#{value}']", wait: 60).click
  end

  def aprova_termo_cessao_digital
    click_button('Continuar', wait: 60)
    sleep 5
    click_button('Li e aprovo a cessão de crédito', wait: 60)
    sleep 10
  end

  def acessa_novo_menu
    find(:xpath, "//*[@id='sel-sistema[1]']/span").click
  end

#daqui pra baixo é só dell --------------------------------------------------------------------------------------------------------------------

  def selecionar_vendedor_dell(vendedor)
    page.has_no_xpath?("div[class='loading-logo']")
    find("span[class='chevron fa fa-chevron-down']", wait: 60).click
      if page.has_xpath?("//a[text()='#{vendedor}']")
        find('a', exact_text: "#{vendedor}").select_option
      else
        find('span', exact_text: 'Adicionar vendedor').click
        find("input[name='vendedor']").set(vendedor)
        find('a', exact_text: 'Finalizar Cadastro').click
      end
      page.has_no_xpath?("div[class='loading-logo']")
  end

  def valor_solicitado
    find("input[formcontrolname='valorSolicitado']").set(gerar_valor(400, 5000))
  end

  def cpf_cliente(cpf)
    $cpf_teste = cpf
    find("input[formcontrolname='cpf']").set(cpf)
  end

  def selecionar_seguro_dell(seguro)
    find('button', text: 'Produtos Omni', wait: 120).click
    find('p', exact_text: "#{seguro}").click
    find('a', exact_text: 'Salvar').click
  end

  def selecionar_parcelas_dell(qtd_parcela)
    $qtd_parcela = qtd_parcela
    find("button[id='#{qtd_parcela}']", wait: 120).click
  end

  def armazenar_dados_proposta_dell
    parcelas_valor_auxiliar = find(:xpath, "//b[text()='#{$qtd_parcela}']").text
    quantidade_parcela_omni_mais = parcelas_valor_auxiliar.gsub('x', '')
    return quantidade_parcela_omni_mais
  end

  def preencher_dados_contato_cliente_dell(nome, telefone)
    $telefone = telefone
    find("input[id='user-name']", wait: 30).set(nome)
    find("input[name='celular']").set(telefone)
    find("input[id='user-email']").set(gerar_email)
    find("input[name='cep']").set('06184140')
  end

  def voltar_para_homepage
    find('a', text:'Voltar para home', wait: 60).click
  end

  def pesquisar_proposta_dell(nome)
    find('a', text: 'Filtrar Fichas').click
    find("input[name='nome']", wait: 60).set(nome)
  end

  def pesquisar_ficha_dell(proposta)
    find('a', text: 'Filtrar Fichas', wait: 120).click
    find("input[name=numero]", wait: 60).set(proposta)
  end

  def acessar_proposta_dell
    find('span', text: 'Verificar Status e Continuar Preenchimento', wait: 120).click
  end

  def preencher_dados_cliente_dell(nascimento, sexo)
    cpf_teste = find("input[name='cpf']", wait: 120).value
    $cpf_teste = cpf_teste.gsub(/[^0-9]/, '')
    find("input[formcontrolname='dataNascimento']", wait: 60).set(nascimento)
    select('BRASILEIRA', from: 'user-citizenship')
    select("#{sexo}", from: 'sexoCliente')
    select('SOLTEIRO', from: 'estadoCivilCliente')
    select('AUTONOMO', from: 'user-occupation-class')
    select('ADMINISTRADOR DE EMPRESAS', from:'profissao')
    find("input[name='renda']").set(gerar_renda(4000, 8000))
    find("input[value='false']").click
    find("input[name='telefone']").set('1198765432')
    find("input[formcontrolname='nomeReferencia']").set('JULIANA AMIGA')
    find("input[formcontrolname='telefoneReferencia']").set('11997543627')
  end

  def preencher_endereco_cliente_dell
    limpar_e_escrever2("input[name='numero']", '123')
    select('RESIDENCIAL', from: 'enderecoCorrespondencia')
  end

  def biometria_dell
    @ddd = '55' + $telefone.split('')[0..1].join('')
    @telefone = $telefone.split('')[2..10].join('')
    find('span', text: 'Reenviar Link').click

    nova_janela = open_new_window
    within_window nova_janela do
      @sms = Sms.new
      @sms.load
      sleep(10)
      @sms.pesquisar_sms(@ddd, @telefone)
      @link = @sms.pegar_link_biometria
      puts @link
    end

    @nova_janela = open_new_window
    within_window @nova_janela do
      biometria = Biometria.new
      biometria.load(link: @link)
      find('label', text: 'CONTINUAR', wait: 60).click
      find("input[name='cpf']").set($cpf_teste)
      find('label', text: 'CONTINUAR', wait: 60).click
      sleep 5
    end
    sleep 80
    find('span', text: 'Reenviar Link', wait: 60).click
    sleep 20
  end

  def verificar_biometria
      verifica = page.has_text?('O cliente realizou todas as etapas da verificação')
      puts verifica
      return verifica
  end

  # funcoes complementares para dell #

  def gerar_valor(inicial = 01, final = 99999)
    valor_dell = rand(inicial..final).to_s + '00'
    return valor_dell
  end

  def gerar_renda(inicial = 01, final = 99999 )
    renda_dell = rand(inicial..final).to_s + '00'
    return renda_dell
  end
end
