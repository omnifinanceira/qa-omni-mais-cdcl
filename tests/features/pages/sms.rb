require_relative 'Util'
require 'pry'

class Sms < Util
  set_url 'https://hmg-api.omni.com.br/sms/messages'

  element :elementinputddd, :xpath, "//input[@id='ddd']"
  element :elementinputtelefone, :xpath, "//input[@id='fone']"
  element :elementbuttonpesquisar, :xpath, "//button[text()='Pesquisa']"

  def pesquisar_sms(ddd, telefone)
    elementinputddd.set ddd
    elementinputtelefone.set telefone
    sleep 10
    elementbuttonpesquisar.click
  end

  def pegar_token
    textosms = all(:xpath, '//tr')[1].all(:xpath, '//td')[3].text
    puts textosms
    token = textosms.gsub(/[^0-9]/, '')
    puts token
    return token
  end

  def pegar_token_cadastro_cessao
    textosms = all(:xpath, '//tr')[0].all(:xpath, '//td')[3].text
    puts textosms
    token = textosms.gsub(/[^0-9]/, '')
    puts token
    return token
  end

  def pegar_link_assinatura
    textosms = all(:xpath, '//tr')[1].all(:xpath, '//td')[3].text
    link = textosms.slice(115, 120)
    sleep 5
    return link
  end

  def pegar_link_biometria
    textosms = all(:xpath, '//tr')[1].all(:xpath, '//td')[3].text
    link = textosms.slice(129, 133)
    sleep 5
    return link
  end
end
