class Util < SitePrism::Page
    def initialize
      $usuariomesa = "FERNANDA_CAPITANAM"
      $senhamesa = "DOG@2022"
      $usuarioloja = "01631LVFLEXTESTE"
      $senhalojista = "DYZWCS68"
      $usuariodell = "01631ldellteste"
      $senhadell = "3QWJHOQP"
      $usuariopremium = "01631L3ARTTESTE"
      $senhapremium = "SENHA123"
      ###################################
     # usuario bipartido
     # $usuariocessao = "02872LNEWLAR"
     # $senhacessao = "senha123"
      $usuariocessao = "02872LMOVEIS1"
      $senhacessao = "TZATBTLG"
      ####################################
      $usuariodigital = "2872NERIO"
      $senhadigital = "MM7UVTIF"
      $numero_proposta = ""
      $contrato = ""
      criar_pasta_log
      $wait = Selenium::WebDriver::Wait.new(:timeout => 60)
    end

    def criar_pasta_log
          @diretorio = "C:/report_automacao"
          Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
  
          @diretorio = "#{Dir.pwd}/reports"
          Dir.mkdir(@diretorio) unless File.exists?(@diretorio)
    end
  
    def limpar_e_escrever(xpath, texto)
      all(:xpath, xpath)[0].native.clear
      all(:xpath, xpath)[0].set(texto)
    end

    def limpar_e_escrever2(xpath, texto)
      find(xpath, wait: 60).native.clear
      find(xpath, wait: 60).set(texto)
    end
  end
  