            #language:pt
            @0 @login
            Funcionalidade: Login no Omni Mais

            @login-sucesso
            Cenario: Quando eu realizo o login com sucesso

            Quando eu preencher o campo <usuario>
            E eu digitar a <senha>
            E eu clicar no botao Entrar
            E eu selecionar o agente
            E eu clicar no botão Confirmar
            Entao eu espero visualizar a home do omni mais

            Exemplos:
            | usuario         | senha      |
            | "184BATISTELLA" | "B3EOP93U" |

            @login-falha
            Cenario: Quando eu realizo o login com usuario nao cadastrado

            Quando eu preencher o campo <usuario>
            E eu digitar a <senha>
            E eu clicar no botao Entrar
            Entao verifico a <mensagem> de retorno

            Exemplos:
            | usuario    | senha    | mensagem                    |
            | "111TESTE" | "000000" | "Usuário ou Senha inválida" |

            @login-senha-falha
            Cenario: Quando eu realizo o login com senha invalida

            Quando eu preencher o campo <usuario>
            E eu digitar a <senha>
            E eu clicar no botao Entrar
            Entao verifico a <mensagem> de retorno

            Exemplos:
            | usuario         | senha    | mensagem                    |
            | "184BATISTELLA" | "000000" | "Usuário ou Senha inválida" |