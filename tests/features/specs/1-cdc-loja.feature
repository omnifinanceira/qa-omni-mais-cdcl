         #language:pt
         @1 @cdc-loja
         Funcionalidade: Criar proposta de CDC Loja

         @loja-seguro-assist  @pipeline
         Cenario: Criar proposta de CDC Loja com seguro e assistencia

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja e assistencia
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via sms com seguro e assistencia e envio para formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-seguro
         Cenario: Criar proposta de CDC Loja com seguro

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via sms com seguro e envio para formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-assist
         Cenario: Criar proposta de CDC Loja com assistencia

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono a assistencia loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via sms com assistencia e envio para formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-nenhum
         Cenario: Criar proposta de CDC Loja sem assistencia e sem seguro

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono nenhuma das opcoes
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via sms sem seguro e sem assitencia e envio para formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-celular
         Cenario: Criar proposta de CDC Loja com seguro e assistencia validando assinatura pelo celular via SMS

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja e assistencia
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via celular com seguro e com assistencia
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-celular-seguro
         Cenario: Criar proposta de CDC Loja com seguro e sem assistencia validando assinatura pelo celular via SMS

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via celular com seguro
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-celular-assist
         Cenario: Criar proposta de CDC Loja sem seguro e com assistencia validando assinatura pelo celular via SMS

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono a assistencia loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via celular com assistencia
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |


         @loja-celular-sem
         Cenario: Criar proposta de CDC Loja sem seguro e sem assistencia validando assinatura pelo celular via SMS

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono nenhuma das opcoes
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via celular
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-email
         Cenario: Criar proposta de CDC Loja com seguro e assistencia validando assinatura pelo celular via Email

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja e assistencia
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via email com seguro e com assistencia
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-email-seguro
         Cenario: Criar proposta de CDC Loja com seguro e sem assistencia validando assinatura pelo celular via Email

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via email com seguro
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-email-assist
         Cenario: Criar proposta de CDC Loja sem seguro e com assistencia validando assinatura pelo celular via Email

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono a assistencia loja
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via email com assistencia
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-email-sem
         Cenario: Criar proposta de CDC Loja sem seguro e sem assistencia validando assinatura pelo celular via Email

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono nenhuma das opcoes
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via email
         E pesquiso o numero da proposta na aba pagamentos
         E envio para a formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento


         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         @loja-nenhum-assincrono
         Cenario: Criar proposta de CDC Loja sem assistencia e sem seguro

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono nenhuma das opcoes
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnifacil com o usuario da mesa
         E acesso o menu mesa de credito
         E pesquiso e acesso a proposta
         E realizo o aceite da proposta
         E valido o checklist e se a proposta foi aprovada
         E acesso o omnimais com o usuario lojista
         E pesquiso o numero da proposta na aba fichas aprovadas
         E acesso a timeline da proposta para fechar negocio
         E preencho os dados complementares
         E anexo os documentos e envio para a formalizacao
         E realizo a assinatura dos documentos via sms sem seguro e sem assitencia e envio para formalizacao
         E acesso o omnifacil com usuario da formalizacao
         E acesso o novo menu
         E seleciono o agente correspondente
         E acesso a fila de formalizacao
         E busco pelo numero do contrato gerado
         E aceito os documentos
         E acesso o menu snv
         E seleciono o agente correspondente
         E pesquiso o contrato no menu snv
         E faco a formalizacao
         Entao acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento

         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         #cenario para teste assincrono
         @assincrono-loja @assincrono
         Cenario: Testar crivo assincrono loja sem seguro e assistencia

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono nenhuma das opcoes
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnimais com o usuario lojista
         Entao verifico se a proposta esta aguardando analise da mesa

         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

         #cenario para teste assincrono
         @assincrono-loja2 @assincrono
         Cenario: Testar crivo assincrono loja com seguro e assistencia

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja e assistencia
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente assincrono
         E valido os dados enviados da proposta
         E acesso o omnimais com o usuario lojista
         Entao verifico se a proposta esta aguardando analise da mesa

         Exemplos:
         | cpf           | nome                       | nascimento   | telefone      | sexo        |
         | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |



         #cenario para teste biometria cliente
         @biometria
         Cenario: Testar assinatura biometria

         Quando logo como cdc loja
         E clico em adicionar ficha
         E clico no tipo de operacao loja
         E seleciono o codigo de operacao
         E seleciono o vendedor
         E preencho as informacoes do cliente <cpf> e <nascimento> loja
         E seleciono o seguro da loja e assistencia
         E seleciono as parcelas em resultado parcial assincrono
         E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
         E valido o token via sms
         E preencho os enderecos do cliente
         E preencho os demais dados do cliente biometria
         E valido os dados enviados da proposta biometria
         E acesso o omnimais com o usuario lojista
         Entao verifico se a proposta esta aguardando analise da mesa

         Exemplos:
         | cpf | nome | nascimento | telefone | sexo |

# | "38642964715" | "MARIA DOS PRAZERES MAGALHAES OLIVEIRA"     | "10/12/1948" | "11989091457" | "FEMININO"  |
# | "35073713753" | "MERCEDES SONEGHETI"           | "07/09/1936" | "11989091457" | "FEMININO"  |
# | "64565777215" | "ERINALDA DANTAS DE LIMA"           | "07/06/1971" | "11989091457" | "MASCULINO"  |
# | "91500966568" | "VALDETE DE JESUS SOUZA"           | "02/09/1971" | "11989091457" | "FEMININO"  |
# | "38712830291" | "MAURINA RIBEIRO DA SILVA BARBOSA" | "26/09/1971" | "11989091457" | "FEMININO"  |
# | "02839026201" | "PAULO CESAR EDUARDO SOARES"       | "27/09/1995" | "11989091457" | "MASCULINO" |
# | "13962604200" | "JOSIAS RODRIGUES DO NASCIMENTO"   | "06/01/1959" | "11989091457" | "MASCULINO" |
# | "63278863291" | "OLGA ZAMPERINI"  | "11/02/1965" | "11989091457" | "FEMININO"  |
# | "06402177296" | " FRANCISCA MARIANA DA SILVA NASCIMENTO"    | "10/02/1999" | "11989091457" | "FEMININO"  |
# | "87267144268" | "DIRCILENE ALTINA CORDEIRO"                 | "06/06/1978" | "11989091457" | "FEMININO"  |
# | "05049740258"| "KESIA SUELEN DA SILVA GONCALVES" |"05/08/2002"| "11989091457" | "FEMININO" |
# | "05109373221"| "KAWAN BRUNO DE SOUZA" |"27/05/2003" |"11989091457" | "MASCULINO" |



