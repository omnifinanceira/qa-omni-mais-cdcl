            #language:pt
            @2 @cdc-premium

            Funcionalidade: Criar proposta de CDC Premium

            @premium @pipeline
            Cenario: Criar proposta de CDC Premium assinando via SMS

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso omnimais como usuario premium
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos premium via sms e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de premium e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

            @premium-celular
            Cenario: Criar proposta de CDC Premium assinando via Celular

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso omnimais como usuario premium
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via celular premium e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de premium e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

            @premium-email
            Cenario: Criar proposta de CDC Premium assinando via Email

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso omnimais como usuario premium
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via email premium e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de premium e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

            @premium-nenhum
            Cenario: Criar proposta de CDC Premium sem nenhum seguro

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso omnimais como usuario premium
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via sms sem seguro e sem assitencia e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de premium e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

            @assincrono-premium @#assincrono
            Cenario: Testar crivo assincrono premium

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium assincrono
            E valido os dados enviados da proposta
            E acesso o omnimais com o usuario premium
            Entao verifico se a proposta esta aguardando analise da mesa

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

