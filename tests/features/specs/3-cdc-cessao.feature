            #language:pt
            @3 @cdc-cessao
            Funcionalidade: Criar proposta de Cessao de Credito

            @cessao @pipeline
            Cenario: Criar proposta de Cessao de Credito

            Quando logo como cessao de credito
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao cessao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta cessao
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario cessao
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos de cessao via sms
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente cessao
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente cessao
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            E acesso o omnimais com usuario de aprovacao digital
            E pesquiso e acesso a proposta no omnimais
            E seleciono a carencia de formalizacao
            E aprovo o termo de cessao
            Entao acesso o omnimais com o usuario de cessao e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091458" | "MASCULINO" |

            @cessao-celular
            Cenario: Criar proposta de Cessao de Credito com assinatura via Celular

            Quando logo como cessao de credito
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao cessao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta cessao
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario cessao
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos de cessao via celular
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente cessao
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente cessao
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            E acesso o omnimais com usuario de aprovacao digital
            E pesquiso e acesso a proposta no omnimais
            E seleciono a carencia de formalizacao
            E aprovo o termo de cessao
            Entao acesso o omnimais com o usuario de cessao e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091460" | "MASCULINO" |

            @cessao-email
            Cenario: Criar proposta de Cessao de Credito com assinatura via Email

            Quando logo como cessao de credito
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao cessao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente assincrono
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta cessao
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario cessao
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos de cessao via email
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente cessao
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente cessao
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            E acesso o omnimais com usuario de aprovacao digital
            E pesquiso e acesso a proposta no omnimais
            E seleciono a carencia de formalizacao
            E aprovo o termo de cessao
            Entao acesso o omnimais com o usuario de cessao e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091458" | "MASCULINO" |

            @envelope
            Cenario: Criar proposta de Cessao de Credito
            #para testar esse cenario precisa chumbar o numero de uma proposta que ja foi aprovada na mesa
            Quando logo como cessao de credito
            E acesso o omnimais com o usuario cessao
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos de cessao via sms
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente cessao
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente cessao
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            E acesso o omnimais com usuario de aprovacao digital
            E pesquiso e acesso a proposta no omnimais
            E seleciono a carencia de formalizacao
            E aprovo o termo de cessao
            Entao acesso o omnimais com o usuario de cessao e verifico se o contrato esta aguardando pagamento


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091458" | "MASCULINO" |


            #cenario para teste biometria cliente
            @biometriacessao
            Cenario: Testar assinatura biometria cessao

            Quando logo como cessao de credito
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao cessao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente biometria
            E valido os dados enviados da proposta biometria
            E acesso o omnimais com o usuario cessao
            E verifico se a proposta esta aguardando analise da mesa
            Entao acesso o omnifacil para verificar se a foto foi anexada a proposta

            Exemplos:
            | cpf           | nome                      | nascimento   | telefone      | sexo       |
            | "37727710504" | "MARIA IZABEL CAVALCANTE" | "06/12/1949" | "11989091458" | "FEMININO" |

            #cenario para teste assincrono
            @assincrono-cessao @assincrono
            Cenario: Testar crivo assincrono cessao

            Quando logo como cessao de credito
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao cessao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> cessao
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente assincrono
            E valido os dados enviados da proposta
            E acesso o omnimais com o usuario cessao
            Entao verifico se a proposta esta aguardando analise da mesa

             Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |