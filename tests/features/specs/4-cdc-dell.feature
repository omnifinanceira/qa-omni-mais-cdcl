            #language:pt
            @4 @cdc-dell
            Funcionalidade: Criar proposta de CDC Dell

            @dell-nenhum @pipeline
            Cenario: Criar proposta de CDC Dell sem seguro validando assinatura via sms

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E nao seleciono nenhum seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via sms sem seguro e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |

            @dell-seguro
            Cenario: Criar proposta de CDC Dell com seguro validando assinatura via sms

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E seleciono o seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via sms com seguro e envio para formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |


            @dell-celular-sem
            Cenario: Criar proposta de CDC Dell sem seguro validando assinatura via celular

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E nao seleciono nenhum seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via celular sem seguro dell
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |


            @dell-celular-seguro
            Cenario: Criar proposta de CDC Dell com seguro validando assinatura via celular

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E seleciono o seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via celular com seguro dell
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |


            @dell-email-sem
            Cenario: Criar proposta de CDC Dell sem seguro validando assinatura via email

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E nao seleciono nenhum seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via email sem seguro dell
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |


            @dell-email-seguro
            Cenario: Criar proposta de CDC Dell com seguro validando assinatura via email

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E seleciono o seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E realizo o aceite da proposta
            E valido o checklist e se a proposta foi aprovada
            E acesso o omnimais com o usuario dell
            E pesquiso o numero da proposta na aba fichas aprovadas
            E acesso a timeline da proposta para fechar negocio
            E preencho os dados complementares
            E anexo os documentos e envio para a formalizacao
            E realizo a assinatura dos documentos via email com seguro dell
            E pesquiso o numero da proposta na aba pagamentos
            E envio para a formalizacao
            E acesso o omnifacil com usuario da formalizacao
            E acesso o novo menu
            E seleciono o agente correspondente
            E acesso a fila de formalizacao
            E busco pelo numero do contrato gerado
            E aceito os documentos
            E acesso o menu snv
            E seleciono o agente correspondente
            E pesquiso o contrato no menu snv
            E faco a formalizacao
            Entao acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento

            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091457" | "MASCULINO" |

            #cenario para teste assincrono
            @assincrono-dell @assincrono
            Cenario: Testar crivo assincrono dell

            Quando logo como cdc dell
            E clico em adicionar ficha
            E seleciono o vendedor dell
            E preencho o valor solicitado
            E preencho o <cpf> do cliente assincrono
            E seleciono a quantidade de parcelas dell
            E nao seleciono nenhum seguro dell
            E preencho os dados para contato do cliente com <nome> e <telefone>
            E volto para a homepage
            E pesquiso e acesso a proposta dell
            E realizo a biometria do cliente
            E preencho os dados do cliente com <nascimento> e <sexo>
            E preencho os enderecos do cliente dell
            E verifico se a biometria foi realizada assincrono
            E valido os dados enviados da proposta dell
            E acesso o omnimais com o usuario dell
            Entao verifico se a proposta esta aguardando analise da mesa

             Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |


