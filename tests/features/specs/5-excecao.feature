            #language:pt
            @excecao-all
            Funcionalidade: CDC-Loja - Regressivo

            @cpf_invalido
            Cenario: Validar campo CPF invalido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes erradas do cliente <cpf> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | telefone      | mensagem       |
            #| "02264465905"| "14/05/1975" | "11989091456" |
            | "85907780530" | "14/05/1975" | "11989091456" | "CPF Inválido" |

            @cpf_embranco
            Cenario: Validar campo CPF em branco


            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho as informacoes do cliente <cpf> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf | nascimento   | telefone      | mensagem                    |
            | " " | "06/03/1980" | "11989091456" | "O campo CPF é obrigatório" |

            @menor-permitido
            Cenario:  Validar a data de nascimento menor que 22 anos

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho o <nascimento> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | telefone      | mensagem                              |
            | "03202749420" | "14/05/1935" | "11989091456" | "A idade deve ser entre 22 e 85 anos" |
            | "03202749420" | "14/05/2000" | "11989091456" | "A idade deve ser entre 22 e 85 anos" |

            @classe-profissional
            Cenario: Validar Classe Profissional não existe

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o <classeProfissional> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | classeProfissional | mensagem                                    |
            | "03202749420" | "06/03/1980" | " "                | "O campo Classe Profissional é obrigatório" |

            @renda
            Cenario: Validar O campo Renda é obrigatório

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o valor da <renda> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | renda | mensagem                      |
            | "03202749420" | "06/03/1980" | " "   | "O campo Renda é obrigatório" |

            @valor-solicitado
            Cenario: Validar O campo Valor solicitado é obrigatório

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o valor solicitado <valorSolicitado> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | valorSolicitado | mensagem                                 |
            | "03202749420" | "06/03/1980" | " "             | "O campo Valor solicitado é obrigatório" |

            @menor-que-permitido
            Cenario: Validar O campo Valor Solicitado menor que o permitido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho um valor menor <menorValor> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | menorValor | mensagem                                              |
            | "03202749420" | "06/03/1980" | "100"      | "O valor solicitado deve ser entre R$ 200 e R$ 75000" |

            @maior-que-permitido
            Cenario: Validar O campo Valor Solicitado maior que o permitido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho um valor maior <maiorValor> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | maiorValor | mensagem                                              |
            | "03202749420" | "06/03/1980" | "9000000"  | "O valor solicitado deve ser entre R$ 200 e R$ 75000" |

            @renda-menor
            Cenario: Validar o campo Renda com valor menor que o solicitado

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho um valor da renda menor <rendaMenor> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | rendaMenor | mensagem                                   |
            | "03202749420" | "06/03/1980" | "40000"    | "O valor mínimo da renda deve ser R$ 600." |

            @tabela
            Cenario: Validar o campo Tabela

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao seleciono o campo <tabela> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | tabela | mensagem                       |
            | "03202749420" | "06/03/1980" | " "    | "O campo tabela é obrigatório" |

            @cep-residencial
            Cenario: Validar CEP residencial inválido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho o cep invalido <cepInvalido> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | cepInvalido | mensagem             |
            | "03202749420" | "06/03/1980" | "11111111"  | "CEP não encontrado" |

            @cep-obrigatorio
            Cenario: Validar o campo não preenchido do CEP

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o campo do CEP <cepObrigatorio> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | cepObrigatorio | mensagem                                         |
            | "03202749420" | "06/03/1980" | " "            | "O campo Endereço residencial CEP é obrigatório" |

            @endereco
            Cenario: Validar o campo não preenchido do endereço residencial

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o campo do endereco loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                         |
            | "03202749420" | "06/03/1980" | "O campo Endereço é obrigatório" |

            @numero-endereco
            Cenario: Validar o número do endereço não preenchido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o numero do endereço <numeroEndereco> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | numeroEndereco | mensagem                       |
            | "03202749420" | "06/03/1980" | " "            | "O campo Número é obrigatório" |

            @bairro
            Cenario: Validar o campo do bairro não preenchido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E nao preencho o bairro <nomeBairro> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | nomeBairro | mensagem                       |
            | "03202749420" | "06/03/1980" | " "        | "O campo Bairro é obrigatório" |

            @valor-alterado
            Cenario: Validar o campo Alterar Valor menor que o permitido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono para alterar o valor <valorAlterado> loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | valorAlterado | mensagem                                               |
            | "03202749420" | "06/03/1980" | "4000"        | "O valor solicitado deve ser entre R$ 200 e R$ 75000." |
            | "03202749420" | "06/03/1980" | "8000000"     | "O valor solicitado deve ser entre R$ 200 e R$ 75000." |

            @parcelamento
            Cenario: Validar o campo Opções de Parcelamento

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E nao seleciono o campo opcoes
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                             |
            | "03202749420" | "06/03/1980" | "Selecione a quantidade de parcelas" |

            @sem_nome
            Cenario: Validar o campo Nome

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preencho o nome
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | semNome | mensagem                     |
            | "03202749420" | "06/03/1980" | " "     | "O campo Nome é obrigatório" |


            @email_invalido
            Cenario: Validar o campo Email

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho com um email invalido <emailInvalido> do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | emailInvalido | mensagem                       |
            | "03202749420" | "06/03/1980" | "email.com"   | "O email informado é inválido" |

            @telefone
            Cenario: Validar o campo do telefone

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preencho o telefone do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                                     |
            | "03202749420" | "14/05/1975" | "Preencha o campo Celular para enviar o token de segurança!" |

            # verificar como sao feitos esses testes sendo que o sistema puxa ao menos 1 opcao por default
            @genero
            Cenario: Validar a seleção Gênero

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao seleciono o <genero> do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | genero | mensagem                       |
            | "03202749420" | "06/03/1980" | " "    | "O campo Gênero é obrigatório" |

            @estadoCivil
            Cenario: Validar a seleção Estado Civil

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao seleciono o estado civil <estadoCivil> do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | estadoCivil | mensagem                             |
            | "03202749420" | "06/03/1980" | " "         | "O campo Estado Civil é obrigatorio" |

            @profissao
            Cenario: Validar a seleção da Profissão

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao seleciono a <profissao> do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | profissao | mensagem                          |
            | "03202749420" | "06/03/1980" | " "       | "O campo Profissão é obrigatório" |
            # verificar como sao feitos esses testes sendo que o sistema puxa ao menos 1 opcao por default

            @empresa-trabalha
            Cenario: Validar o campo empresa que trabalha

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preencho o campo empresa que trabalho do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                |
            | "03202749420" | "21/03/1980" | "O campo nome da empresa é obrigatório" |

            @telefone-empresa
            Cenario: Validar o campo do telefone da empresa que trabalha

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preencho o campo do telefone da empresa que trabalha do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                    |
            | "03202749420" | "21/03/1980" | "O campo Telefone da empresa é obrigatório" |


            @telefone-adicionais
            Cenario: Validar o campo telefones adicionais

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preencho o campo do telefone adicionais da empresa
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                         |
            | "03202749420" | "06/03/1980" | "O campo Telefone é obrigatório" |

            @referencia-nome
            Cenario: Validar o campo referencia nome

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preeencho o campo do nome de referencia loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                   |
            | "03202749420" | "06/03/1980" | "O campo Nome da Referência é Obrigatório" |

            @referencia-telefone
            Cenario: Validar o campo referencia telefone

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E nao preeencho o campo do telefone de referencia loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                       |
            | "03202749420" | "06/03/1980" | "O campo Telefone da Referência é Obrigatório" |

            @fichaAnalise
            Cenario: Validar o campo Ficha em Análise

            Quando logo como cdc loja
            E clico em Fichas Aprovadas
            E clico em Fichas em Analise
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem            |
            | "RESULTADO PARCIAL" |
            | "EM ANDAMENTO"      |

            @fichaAprovadas
            Cenario: Validar as informações da ficha Aprovada

            Quando logo como cdc loja
            E clico em Fichas Aprovadas
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem   |
            | "APROVADA" |

            @fichaRecusadas
            Cenario: Validar as informações da ficha Recusada

            Quando logo como cdc loja
            E clico em Recusadas
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem   |
            | "RECUSADA" |

            @emPreenchimento
            Cenario: Validar as informações da ficha Em Preenchimento

            Quando logo como cdc loja
            E clico em Preenchimento
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem           |
            | "EM PREENCHIMENTO" |

            @recarregar
            Cenario: Validar o campo Recarregar

            Quando logo como cdc loja
            E clico em Recarregar
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem           |
            | "EM PREENCHIMENTO" |

            @contratoCancelado
            Cenario: Validar a aba Contrato cancelado

            Quando logo como cdc loja
            E clico em Contrato cancelado
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem             |
            | "CONTRATO CANCELADO" |

            @produtosCDC
            Cenario: Validar a aba CDC LOJA

            Quando logo como cdc loja
            E clico em CDC LOJA
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem   |
            | "CDC LOJA" |

            @todos
            Cenario: Validar a aba TODOS

            Quando logo como cdc loja
            E clico em TODOS
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem |
            | "TODOS"  |

            @nomeLoja
            Cenario: Validar o campo Filtrar Fichas: Nome da loja

            Quando logo como cdc loja
            E clico em filtrar Fichas
            E insiro o <nomeLoja> da loja
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | nomeLoja             | mensagem                          |
            | "Vitalflex Colchoes" | "Vitalflex Com. De Colchoes Ltda" |

            #verificar como vai rodar a bateria de testes cpf nao pode ter base na omni
            @campoComercial
            Cenario: Validar o preenchimento do campo CEP Comercial



            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencher info cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes e <telefone> e <nome> e <sexo>
            E clicar em continuar
            E nao preencho o informacoes comercial <enderecoComercial> e <numeroComercial> e <bairro>
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nome                              | nascimento   | classeProfissional                | telefone      | sexo       | enderecoComercial | numeroComercial | bairro    | mensagem                                       |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "SOCIO, PROPRIETARIO, EMPRESARIO" | "11999999999" | "FEMININO" | " "               | "66 "           | "OSASCO " | "O campo Endereço comercial CEP é obrigatório" |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "SOCIO, PROPRIETARIO, EMPRESARIO" | "11999999999" | "FEMININO" | "01030000 "       | " "             | "CENTRO"  | "O campo Número é obrigatório"                 |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "SOCIO, PROPRIETARIO, EMPRESARIO" | "11999999999" | "FEMININO" | "01030000 "       | "02 "           | " "       | "O campo Bairro é obrigatório"                 |

            @maisProduto
            Cenario: Validar proposta com mais de um produto

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono a assistencia loja
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente teste
            E adiciono mais de um produto teste
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nome                              | nascimento   | telefone      | sexo        | mensagem                                                  |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "11999999999" | "MASCULINO" | "Valor total dos produtos diferente do valor solicitado!" |

            @maiorValor
            Cenario: Validar produto com valor maior que o permitido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono a assistencia loja
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente teste
            E adiciono mais de um produto teste
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nome                              | nascimento   | telefone      | sexo        | mensagem                                                  |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "11999999999" | "MASCULINO" | "Valor total dos produtos diferente do valor solicitado!" |

            @menorValor
            Cenario: Validar produto com valor maior que o permitido

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono a assistencia loja
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente teste
            E adiciono mais de um produto teste
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nome                              | nascimento   | telefone      | sexo        | mensagem                                                  |
            | "03202749420" | "CARLOS EDUARDO LOPES DOS SANTOS" | "06/03/1980" | "11999999999" | "MASCULINO" | "Valor total dos produtos diferente do valor solicitado!" |

            @codigoOperacao
            Cenario: Validar código de Operação

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem                    |
            | "8472 - CDCL APP 2 C/J SUL" |

            @nomeVendedor
            Cenario: Validar o Campo Nome do Vendedor

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono vendedor
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | mensagem    |
            | "Continuar" |

            @doLar
            Cenario: Validar opção Cliente com Classe profissional Do lar

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho o <cpf> e <nascimento>
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | classeProfissional             | mensagem                 |
            | "42053615800" | "07/09/1994" | "OUTRO DONA DE CASA ESTUDANTE" | "Informações do Cônjuge" |

            @infoConjuge
            Cenario: Validar as Infomaçoes do Cônjuge / cpf

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho o <cpf> e <nascimento>
            E preencho os detalhes do Conjugue <cpf> e <nascimento>
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem                                           |
            | "07468219486" | "29/11/1987" | "o CPF do conjuge não pode ser o mesmo do cliente" |

            @conjugeSalvo
            Cenario: Validar as informações salvas do Cônjuge

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho o <cpf> e <nascimento>
            E preencho as informacoes do Conjugue <cpf_conjuge> e <nascimento>
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | cpf_conjuge   | nascimento   | tabela                       | mensagem    |
            | "07468219486" | "34210454885" | "01/02/1981" | "TABELA AMARELO FINOS 2 SUL" | "Continuar" |

            @dataVencimento
            Cenario: Validar data de vencimento

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o vencimento
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | mensagem       |
            | "07468219486" | "29/11/1987" | "Vencimento: " |

            #------------------------------leo daqui pra baixo-------------------------------------------#
            @fichas-recusadas
            Cenario: Validar aba fichas recusadas

            Quando logo como cdc loja
            E clico na aba fichas recusadas
            Entao o sistema devera mostrar todas as fichas <situacao>

            Exemplos:
            | situacao   |
            | "RECUSADA" |

            @fichas-analise-cpf
            Cenario: Validar aba fichas em analise pelo cpf

            Quando logo como cdc loja
            E clico em filtrar fichas
            E insiro o <cpf> do cliente
            Entao o sistema devera filtrar as fichas para achar aquela com o <cpf> inserido

            Exemplos:
            | nome                         | cpf           |
            | "LUCINEIA REGINA DOS SANTOS" | "02264465905" |

            #testar 11/02/2022
            @valor-mercadoria
            Cenario: Criar proposta de CDC Loja validando campo valor do produto

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>

            Exemplos:
            | cpf           | nascimento   | telefone      | nome                         | sexo       | mensagem                                 |
            | "02264465905" | "14/05/1975" | "11989091456" | "LUCINEIA REGINA DOS SANTOS" | "FEMININO" | "O campo Valor do Produto é obrigatório" |

            @recusa-mesa-loja
            Cenario: Criar proposta de CDC Loja e recusar na mesa de credito

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E nao realizo o aceite da proposta
            E logo como cdc loja
            E clico na aba fichas recusadas
            E clico em filtrar fichas
            E pesquiso a proposta
            Entao o sistema devera mostrar a ficha <situacao>

            Exemplos:
            | cpf           | nascimento   | telefone      | nome                         | sexo       | situacao   |
            | "02264465905" | "14/05/1975" | "11989091456" | "LUCINEIA REGINA DOS SANTOS" | "FEMININO" | "RECUSADA" |

            #mapeado porem sem rodar, precisa resolver impedimentos
            @recusa-mesa-dell
            Cenario: Criar proposta de CDC Dell e recusar na mesa de credito

            Quando logo como cdc dell
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E nao realizo o aceite da proposta
            E logo como cdc loja
            E clico na aba fichas recusadas
            E clico em filtrar fichas
            E pesquiso a proposta
            Entao o sistema devera mostrar a ficha <situacao>

            Exemplos:
            | cpf           | nascimento   | telefone      | nome                         | sexo       | situacao   |
            | "02264465905" | "14/05/1975" | "11989091456" | "LUCINEIA REGINA DOS SANTOS" | "FEMININO" | "RECUSADA" |

            #mapeado porem nao testado ambiente travando
            @recusa-mesa-premium
            Cenario: Criar proposta de CDC Premium e recusar na mesa de credito

            Quando logo como cdc premium
            E clico em adicionar ficha
            E clico no tipo de operacao premium
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium
            E adiciono um produto
            E valido os dados enviados da proposta
            E acesso o omnifacil com o usuario da mesa
            E acesso o menu mesa de credito
            E pesquiso e acesso a proposta
            E nao realizo o aceite da proposta
            E logo como cdc premium
            E clico na aba fichas recusadas
            E clico em filtrar fichas
            E pesquiso a proposta
            Entao o sistema devera mostrar a ficha <situacao>

            Exemplos:
            | cpf           | nascimento   | telefone      | nome                         | sexo       | situacao   |
            | "02264465905" | "14/05/1975" | "11989091456" | "LUCINEIA REGINA DOS SANTOS" | "FEMININO" | "RECUSADA" |

            @endereco-correspondencia
            Cenario: Criar proposta de CDC Loja com seguro e assistencia e validar endereco correspondencia

            Quando logo como cdc loja
            E clico em adicionar ficha
            E clico no tipo de operacao loja
            E seleciono o codigo de operacao
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> loja exec
            E seleciono o seguro da loja e assistencia
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E nao preencho os enderecos do cliente
            Entao O sistema devera apresentar a mensagem de <mensagem>


            Exemplos:
            | cpf           | nascimento   | telefone      | nome                         | sexo       | mensagem                                              |
            | "02264465905" | "14/05/1975" | "11989091456" | "LUCINEIA REGINA DOS SANTOS" | "FEMININO" | "O campo Endereço para correspondência é obrigatório" |

            @premium-addproduto
            Cenario: Criar proposta para mais de um produto

            Quando logo como cdc premium
            E clico em adicionar ficha
            E seleciono o codigo de operacao premium
            E seleciono o vendedor
            E preencho as informacoes do cliente <cpf> e <nascimento> premium
            E seleciono as parcelas em resultado parcial assincrono
            E preencho os detalhes do cliente e <telefone> e <nome> e <sexo>
            E valido o token via sms
            E preencho os enderecos do cliente
            E preencho os demais dados do cliente premium
            E adiciono mais de um produto
            Entao valido os dados enviados da proposta


            Exemplos:
            | cpf           | nome                       | nascimento   | telefone      | sexo        |
            | "03429965845" | "NELSI BATISTA DO AMARAL	" | "23/06/1997" | "11989091456" | "MASCULINO" |

