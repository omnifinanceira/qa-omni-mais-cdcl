require 'pry'

Quando('logo como cessao de credito') do
  @loginomnifacil = LoginOmniFacil.new
  @omnifacil = OmniFacil.new
  @login = Login.new
  @home = Home.new
  @omnimais = OmniMais.new
  @excecao = Excecao.new
  @login.load
  @login.acessar_conta_omnimais_cessao
end

Quando('preencho as informacoes do cliente {string} e {string} cessao') do |cpf, nascimento|
  @omnimais.preencher_dados_cliente_loja(cpf,nascimento, 'TABELA CESSÃO')
  @omnimais.clicar_em_continuar  
  sleep(5) 
end

Quando('realizo a assinatura dos documentos de cessao via sms') do
@omnimais.assinatura_digital_cessao
@omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")    
end

Quando('realizo a assinatura dos documentos de cessao via email') do
  @omnimais.assinatura_digital_email_cessao
end

Quando('realizo a assinatura dos documentos de cessao via celular') do
  @omnimais.assinatura_digital_celular_cessao
end

Quando('seleciono a carencia de formalizacao') do
  @omnimais.selecionar_carencia('1')
end

Quando('seleciono o agente correspondente cessao') do
  @omnifacil.listar_agente_cessao
end

Quando('seleciono o codigo de operacao cessao') do
  @omnimais.selecionar_operacao_cessao('8995')
  @omnimais.clicar_em_continuar_entrada
end

Quando('aprovo o termo de cessao') do
  @omnimais.aprova_termo_cessao_digital
end

Quando('acesso o omnimais com usuario de aprovacao digital') do
  @login.load
  @login.acessar_conta_omnimais_cessao_digital
end

Quando('pesquiso e acesso a proposta no omnimais')do
  #$proposta = '31679601'
  @omnimais.pesquisar_proposta_digital($proposta)
end

Quando('pesquiso e acesso a proposta cessao') do
  @omnifacil.pesquisar_numero_proposta_cessao($proposta)
end

Quando('acesso o omnimais com usuario de cessao') do
  @login.load
  @login.acessar_conta_omnimais_cessao
end

Entao('acesso o omnimais com o usuario de cessao e verifico se o contrato esta aguardando pagamento') do
  @login.load
  @login.acessar_conta_omnimais_cessao
  @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
  @omnimais.acessar_aba_pagamentos
  @omnimais.pesquisar_proposta_valida
  proposta = @excecao.valida_ficha_numero
  expect((proposta).match($proposta)).to be_truthy
end

Entao('acesso o omnifacil para verificar se a foto foi anexada a proposta') do
  @loginomnifacil.load 
  @loginomnifacil.acessar_conta_omnifacil_mesa
  @omnifacil.listar_agente_cessao
  @omnifacil.selecionar_menu('OPERACIONAL')
  @omnifacil.selecionar_submenu('OMNIDOC')
  @omnifacil.selecionar_submenu('Omni Doc')
  @omnifacil.selecionar_submenu('Visualizar Imagens')
  texto_valida = "Foto do Cliente" 
  texto_valida.gsub(/[[:space:]]/, '').downcase
  texto_esperado = @omnifacil.pesquisa_proposta_para_verificar_imagem
  expect((texto_valida)).match((texto_esperado)).to be_truthy
end

