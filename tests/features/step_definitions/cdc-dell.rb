require 'pry'
Quando('logo como cdc dell') do
    @loginomnifacil = LoginOmniFacil.new
    @omnifacil = OmniFacil.new
    @login = Login.new
    @home = Home.new
    @omnimais = OmniMais.new
    @excecao = Excecao.new
    @login.load
    @login.acessar_conta_omnimais_dell

  end

Quando('seleciono o vendedor dell') do
    @omnimais.selecionar_vendedor_dell('vendedor10@dell.com.br')
end

Quando('preencho o valor solicitado') do
    @omnimais.valor_solicitado
end

Quando('preencho o {string} do cliente') do | cpf |
    @omnimais.cpf_cliente(cpf)
    @omnimais.clicar_em_simular
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('preencho o {string} do cliente assincrono') do | cpf |
    @omnimais.cpf_cliente(cpf)
    @omnimais.clicar_em_simular
    @omnimais.analise_assincrona
end

Quando('seleciono o seguro dell') do
    @omnimais.selecionar_seguro_dell('SEGURO OMNI PROTEÇÃO FINANCEIRA - PREMIUM VAREJO_B')
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    @omnimais.clicar_em_continuar_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('nao seleciono nenhum seguro dell') do
    @omnimais.selecionar_seguro_dell('NENHUM')
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    @omnimais.clicar_em_continuar_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('seleciono a quantidade de parcelas dell') do
    @omnimais.selecionar_parcelas_dell('12x')
    @omnimais.armazenar_dados_proposta_dell
end

Quando('preencho os dados para contato do cliente com {string} e {string}') do |nome, telefone|
    @omnimais.preencher_dados_contato_cliente_dell(nome, telefone)
    @omnimais.clicar_em_enviar_proposta_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('volto para a homepage') do
    $proposta = find("span[id='idProposta']", wait: 120).text
    @omnimais.voltar_para_homepage
end

Quando('pesquiso e acesso a proposta dell') do
    @omnimais.pesquisar_ficha_dell($proposta)
    @omnimais.acessar_proposta_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('realizo a biometria do cliente') do
    @omnimais.biometria_dell
end

Quando('preencho os dados do cliente com {string} e {string}') do |nascimento, sexo|
    @omnimais.preencher_dados_cliente_dell(nascimento, sexo)
    @omnimais.clicar_em_continuar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")
end

Quando('preencho os enderecos do cliente dell') do
    @omnimais.preencher_endereco_cliente_dell
    @omnimais.clicar_em_continuar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('verifico se a biometria foi realizada') do
    verificacao = @omnimais.verificar_biometria
    expect(verificacao).to be_truthy
    @omnimais.clicar_em_analisar_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
end

Quando('verifico se a biometria foi realizada assincrono') do
    verificacao = @omnimais.verificar_biometria
    expect(verificacao).to be_truthy
    @omnimais.clicar_em_analisar_dell
    @omnimais.analise_assincrona
end

Quando('valido os dados enviados da proposta dell') do
    $proposta = find("span[class='ficha-id']", wait: 120).text
    dados_proposta_sucesso = find(:xpath, "//div[4]/form/div/div/p/small").text.split(" ")
    expect((dados_proposta_sucesso[3].gsub('(','').gsub('x','')).match("12")).to be_truthy
    @omnimais.clicar_em_continuar
end

Quando('acesso o omnimais com o usuario dell') do
    @login.load
    @login.acessar_conta_omnimais_dell
end

Quando('realizo a assinatura dos documentos via sms sem seguro e envio para formalizacao') do
    @omnimais.assinatura_digital_nenhum
end

Quando('realizo a assinatura dos documentos via celular com seguro dell') do
    @omnimais.assinatura_digital_celular_dell
end

Quando('realizo a assinatura dos documentos via celular sem seguro dell') do
    @omnimais.assinatura_digital_celular_dell
end

Quando('realizo a assinatura dos documentos via email sem seguro dell') do
    @omnimais.assinatura_digital_email_dell
end

Quando('realizo a assinatura dos documentos via email com seguro dell') do
    @omnimais.assinatura_digital_email_dell
end

Entao('acesso o omnimais com o usuario de dell e verifico se o contrato esta aguardando pagamento') do
    @login.load
    @login.acessar_conta_omnimais_dell
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.acessar_aba_pagamentos
    @omnimais.pesquisar_proposta_valida
    proposta = @excecao.valida_ficha_numero
    expect((proposta).match($proposta)).to be_truthy
end












