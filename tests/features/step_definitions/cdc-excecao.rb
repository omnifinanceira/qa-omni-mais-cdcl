  Quando('preencho as informacoes erradas do cliente {string} loja') do |cpf|
    @excecao.preencher_cpf_invalido(cpf) 
  end

  Quando('nao preencho as informacoes do cliente {string} loja') do |cpf|
    @excecao.sem_info(cpf)
  end 

  Quando('preencho as informacoes do cliente {string} loja') do |cpf|
    @excecao.preencher_cpf(cpf)
  end
  
  Quando('preencho o {string} loja') do | nascimento| 
    @excecao.preencher_data_nascimento(nascimento)
  end

  Quando('nao preencho o {string} loja') do |classeProfissional|
    @excecao.preencher_classe_profissional(classeProfissional)
  end

  Quando('nao preencho o valor da {string} loja') do |renda|
    @excecao.preencher_valor_renda(renda)
  end

  Quando('nao preencho o valor solicitado {string} loja') do |valorSolicitado|
    @excecao.preencher_valor_solicitado(valorSolicitado)
  end

  Quando('preencho um valor menor {string} loja') do |menorValor|
    @excecao.preencher_valor_menor(menorValor)
  end

  Quando('preencho um valor maior {string} loja') do |maiorValor|
    @excecao.preencher_valor_maior(maiorValor)
  end

  Quando('preencho um valor da renda menor {string} loja') do |rendaMenor|
    @excecao.preencher_menor_valor_renda(rendaMenor)
  end

  Quando('nao seleciono o campo {string} loja') do |tabela|
    @excecao.nao_preencher_campo(tabela)
  end

  Quando('preencho o cep invalido {string} loja') do |cepInvalido|
    @excecao.cep_invalido(cepInvalido)
  end

  Quando('nao preencho o campo do CEP {string} loja') do |cepObrigatorio|
    @excecao.cep_obrigatorio(cepObrigatorio)
  end

  Quando('nao preencho o campo do endereco loja') do
    @excecao.endereco_obrigatorio
  end

  Quando('nao preencho o numero do endereço {string} loja') do |numeroEndereco|
    @excecao.numero_obrigatorio(numeroEndereco)
  end

  Quando('nao preencho o bairro {string} loja') do |nomeBairro|
    @excecao.bairro(nomeBairro)
  end

  Quando('seleciono para alterar o valor {string} loja') do |valorAlterado|
    @excecao.valor_alterado(valorAlterado)
  end

  Quando('nao seleciono o campo opcoes') do 
    @excecao.nao_preencho_parcelamento
  end

  Quando('nao preencho o nome') do
    @excecao.nao_preencher_nome_cliente_loja
  end

  Quando('preencho com um email invalido {string} do cliente') do |emailInvalido|
    @excecao.preencher_email_invalido(emailInvalido)
  end

  Quando('nao preencho o telefone do cliente') do 
    @excecao.nao_preencher_telefone
  end

  Quando('nao seleciono o {string} do cliente') do |genero|
    @excecao.nao_selecionar_genero(genero)
  end
 

  Quando('nao seleciono o estado civil {string} do cliente') do |estadoCivil|
    @excecao.nao_selecionar_estado_civil(estadoCivil)
  end
 

  Quando('nao seleciono a {string} do cliente') do |profissao|
    @excecao.nao_preencher_profissao(profissao)
  end
 

  Quando('nao preencho o campo empresa que trabalho do cliente') do
    @excecao.nao_preencho_empresa_trabalho
  end

  Quando('nao preencho o campo do telefone da empresa que trabalha do cliente') do
    @excecao.nao_preencho_telefone_empresa
  end

  Quando('nao preencho o campo do telefone adicionais da empresa') do
    @excecao.nao_preencho_telefone_adicional
  end

  Quando('nao preeencho o campo do nome de referencia loja') do
    @excecao.nao_preencho_nome_referencia
  end

  Quando('nao preeencho o campo do telefone de referencia loja') do 
    @excecao.nao_preencho_telefone_referencia
  end

  Quando('preencho os detalhes e {string} e {string} e {string}') do |telefone, nome, sexo|
    @excecao.preencher_detalhes(telefone, nome, sexo)
  end

  Quando('nao preencho o informacoes comercial {string} e {string} e {string}') do |enderecoComercial, numeroComercial, bairro|
    @excecao.nao_preencher_info_comercial(enderecoComercial, numeroComercial, bairro)
  end

  Quando('preencher info cliente {string} e {string} loja') do |cpf, nascimento|
    @excecao.preencher_info_cliente_loja(cpf, nascimento, 'TABELA AMARELO FINOS 2 SUL')
  end

  Quando('clico em Recarregar') do
    @excecao.carregar_pagina
  end

  Quando('clico em Contrato cancelado') do
    @excecao.mostrar_fichas_canceladas
  end

  Quando('clico em CDC LOJA') do
    @excecao.mostrar_fichas_cdc_loja
  end

  Quando('clico em TODOS') do
    @excecao.mostrar_todos
  end

  Quando('clico em filtrar Fichas') do
    @excecao.mostrar_campo_para_preencher
  end

  Quando('insiro o {string} da loja') do |nomeLoja|
    @excecao.preencher_Nome_loja(nomeLoja)
  end


  Quando('clicar em continuar') do
    @omnimais.clicar_em_continuar
    @excecao.continuar_sem_validar_sms
    
  end

  Quando('adiciono um produto com erro {string}') do |valorProduto|
    @excecao.adicionar_produto(valorProduto)
  end

  Quando('clico em Fichas Aprovadas') do 
    @excecao.mostrar_fichas_aprovadas
  end

  Quando('clico em Fichas em Analise') do
    @excecao.mostrar_fichas_analise
  end

  Quando('clico em Recusadas') do
    @excecao.mostrar_recusadas
  end

  Quando('preencho os demais dados do cliente assincrono teste') do
    @excecao.preencher_demais_dados_cliente_teste
  end

  Quando('clico em Preenchimento') do
    @excecao.mostrar_emPreenchimento
  end

  Quando('adiciono mais de um produto teste') do
    @excecao.adicionar_protudo_teste
  end

  Quando('clico no tipo de operacao') do
    @excecao.selecionar_opcao('CDC Loja')
    @excecao.selecionar_operacao_t('8472 - CDCL APP 2 C/J SUL')
  end

  Quando('seleciono vendedor') do
    @excecao.selecionar_vendedor_loja_t
  end

  Quando('preencho o {string} e {string}') do |cpf, nascimento|
    @excecao.preencher_dolar(cpf, nascimento, 'OUTRO DONA DE CASA, ESTUDANTE')
  end

  Quando('preencho os detalhes do Conjugue {string} e {string}') do |cpf, nascimento |
    @excecao.preencher_dolar(cpf, nascimento, 'OUTRO DONA DE CASA, ESTUDANTE')
    @excecao.preencher_conjuge(cpf, nascimento)
  end

  Quando ('preencho as informacoes do Conjugue {string} e {string}') do |cpf_conjuge, nascimento|
    @excecao.preencher_infoConjuge(cpf_conjuge, nascimento, 'TABELA AMARELO FINOS 2 SUL')
  end

  Quando ('seleciono o vencimento') do
    @excecao.selecionar_data_vencimento
  end
  

  #----------------------------------------leo daqui pra baixo-----------------------------#
  Quando('clico em continuar') do
    @omnimais.clicar_em_continuar
  end

  Quando('clico na aba fichas recusadas') do
    @excecao.acessar_aba_recusada
  end

  Quando('clico na aba fichas aprovadas') do
    @excecao.acessar_aba_aprovada
  end

  Quando('clico na aba fichas em analise') do
    @excecao.acessar_aba_analise
  end

  Quando('clico em filtrar fichas') do
    @excecao.filtrar_fichas
  end

  Quando('insiro o {string} do cliente') do |cpf|
    @excecao.pesquisar_ficha_cpf(cpf)
  end

  Quando('pesquiso a proposta') do
    @excecao.pesquisar_ficha
  end

  Quando('nao realizo o aceite da proposta') do
    @excecao.nao_aprovacao_proposta
    @excecao.motivo_recusa
  end

  Quando('nao preencho os enderecos do cliente') do
    @excecao.nao_selecionar_endereco_correspondencia
  end

  Quando('preencho as informacoes do cliente {string} e {string} loja exec') do |cpf, nascimento|
    @excecao.preencher_dados_cliente_loja_exec(cpf,nascimento, 'TABELA AMARELO FINOS 2 SUL')		
    @omnimais.clicar_em_continuar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
  end

  Entao('o sistema devera mostrar todas as fichas {string}') do |situacao|
    situacao_r = @excecao.verificar_situacao_ficha(situacao)
    expect(situacao_r).to be_truthy
  end

  Entao('o sistema devera mostrar a ficha {string}') do |situacao|
    ficha_r = @excecao.valida_numero_ficha
    situacao_r = @excecao.verificar_situacao_ficha(situacao)
    expect((ficha_r).match($proposta)).to be_truthy
    expect(situacao_r).to be_truthy
  end

  Entao('o sistema devera filtrar as fichas para achar aquela com o {string} inserido') do |cpf|
    cpf_r = @excecao.verificar_filtro_x_pagina
    expect((cpf_r).match(cpf)).to be_truthy
  end

  Entao('O sistema devera apresentar a mensagem de {string}') do |mensagem|
    texto_r = @excecao.verificar_excecao(mensagem)
    expect(texto_r).to be_truthy
  end


