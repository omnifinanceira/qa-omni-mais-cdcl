Quando('logo como cdc loja') do
    @loginomnifacil = LoginOmniFacil.new
    @omnifacil = OmniFacil.new
    @login = Login.new
    @home = Home.new
    @omnimais = OmniMais.new
    @excecao = Excecao.new
    @login.load
    @login.acessar_conta_omnimais_loja
  end
    
  Quando('clico em adicionar ficha') do
    @home.acessar_ficha
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
  end
    
  Quando('clico no tipo de operacao loja') do
    @omnimais.selecionar_opcao('CDC Loja')
  end
  
  Quando('seleciono o codigo de operacao') do
    @omnimais.selecionar_operacao('8472 - CDCL APP 2 C/J SUL')
    @omnimais.clicar_em_continuar_entrada
  end
    
  Quando('seleciono o vendedor') do
    @omnimais.selecionar_vendedor_loja
    @omnimais.clicar_em_continuar
  end
    
  Quando('preencho as informacoes do cliente {string} e {string} loja') do |cpf, nascimento|
    @omnimais.preencher_dados_cliente_loja(cpf,nascimento, 'TABELA AMARELO FINOS 2 SUL')		
    @omnimais.clicar_em_continuar
  end
  
  Quando('seleciono a assistencia loja') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.selecionar_assistencia('ASSIST RESID  CAÇAMBA  CHECK UP 12 MESES MONDIAL')
  end 

  Quando('seleciono nenhuma das opcoes') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.selecionar_nenhum
  end

  Quando('seleciono o seguro da loja') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.selecionar_seguro('SEGURO OMNI PROTEÇÃO FINANCEIRA - PREMIUM VAREJO_B')
  end 

  Quando('seleciono o seguro da loja e assistencia') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.selecionar_seguro_assistencia('SEGURO OMNI PROTEÇÃO FINANCEIRA - PREMIUM VAREJO_B', 'ASSIST RESID  CAÇAMBA  CHECK UP 12 MESES MONDIAL') 
  end 
 
  Quando('seleciono as parcelas em resultado parcial') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")   
    @omnimais.selecionar_parcelas_loja('P12')
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    @omnimais.armazenar_dados_proposta
    @omnimais.clicar_em_analisar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")   
  end

  Quando('seleciono as parcelas em resultado parcial assincrono') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")   
    @omnimais.selecionar_parcelas_loja('P12')
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']") 
    @omnimais.armazenar_dados_proposta
    @omnimais.clicar_em_analisar
    retorno_botao = @omnimais.analise_assincrona
    expect(retorno_botao).to be_falsey
  end
    
  Quando('preencho os detalhes do cliente e {string} e {string} e {string}') do |telefone, nome, sexo|
    @omnimais.preencher_detalhes_cliente_loja(telefone, nome, sexo)
  end
    
  Quando('valido o token via sms') do
    @omnimais.validar_sms
    @omnimais.clicar_em_continuar
  end
    
  Quando('preencho os enderecos do cliente') do
    @omnimais.selecionar_endereco_correspondencia
    @omnimais.clicar_em_continuar
    sleep(5)
  end
    
  Quando('preencho os demais dados do cliente assincrono') do
    @omnimais.anexar_foto
    @omnimais.preencher_demais_dados_cliente_loja
    @omnimais.clicar_em_analisar 
  end

  Quando('preencho os demais dados do cliente assincrono assincrono') do
    @omnimais.anexar_foto
    @omnimais.preencher_demais_dados_cliente_loja
    @omnimais.clicar_em_analisar
    retorno_botao = @omnimais.analise_assincrona
    expect(retorno_botao).to be_falsey
  end

  Quando('preencho os demais dados do cliente assincrono biometria') do #teste assistido
    @omnimais.preencher_demais_dados_cliente_loja_biometria
    @omnimais.clicar_em_analisar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")    
  end
    
  Quando('valido os dados enviados da proposta') do
    $proposta = find(:xpath, "//span[@class='ficha-id']", wait: 60).text
    find(:xpath, "//app-analise/div[1]/div[1]/p").text
    dados_proposta_sucesso = find(:xpath, "//div[4]/form/div/div/p/small").text.split(" ")
    expect((dados_proposta_sucesso[3].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
    expect((dados_proposta_sucesso[7]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy
    puts $proposta
    @omnimais.clicar_em_continuar
  end

  Quando('valido os dados enviados da proposta biometria') do
    $proposta = find(:xpath, "//span[@class='ficha-id']").text
    find(:xpath, "//app-analise/div[1]/div[1]/p").text
    dados_proposta_sucesso = find(:xpath, "//div[4]/form/div/div/p/small").text.split(" ")
    expect((dados_proposta_sucesso[3].gsub('(','').gsub('x','')).match($quantidade_parcela_omni_mais)).to be_truthy
    expect((dados_proposta_sucesso[7]).gsub(')','').match($valor_parcela_omni_mais)).to be_truthy
    @omnimais.clicar_em_continuar
  end
  
  Quando('acesso o omnifacil com o usuario da mesa') do
    @loginomnifacil.load 
    @loginomnifacil.acessar_conta_omnifacil_mesa 
  end
  
  Quando('acesso o menu mesa de credito') do
    @omnifacil.acessar_mesa_credito
    sleep (5)
  end
  
  Quando('pesquiso e acesso a proposta') do
    @omnifacil.pesquisar_numero_proposta($proposta)
  end
  
  Quando('realizo o aceite da proposta') do
    @omnifacil.aprovacao_proposta
  end
    
  Quando('valido o checklist e se a proposta foi aprovada') do
    @omnifacil.preencher_check_list
  end
    
  Quando('acesso o omnimais com o usuario lojista') do
    @login.load
    @login.acessar_conta_omnimais_loja
  end

  Quando('acesso o omnimais com o usuario premium') do
    @login.load
    @login.acessar_conta_omnimais_premium
  end
  
  Quando('acesso o omnimais com o usuario cessao') do
    @login.load
    @login.acessar_conta_omnimais_cessao
  end
    
  Quando('pesquiso o numero da proposta na aba fichas aprovadas') do
    # para chumbar proposta habilitar essa variavel
    # $proposta = ''
    @omnimais.acessar_aba_fichas_aprovadas
    @omnimais.pesquisar_e_acessar_proposta
  end

  Quando('pesquiso o numero da proposta na aba pagamentos') do
    @omnimais.acessar_aba_fichas_pagamentos
    @omnimais.pesquisar_e_acessar_proposta
  end 

  Quando('acesso a timeline da proposta para fechar negocio') do
    @omnimais.fechar_negocio
  end
    
  Quando('preencho os dados complementares') do
    @omnimais.preencher_dados_complementares  
  end
    
  Quando('anexo os documentos e envio para a formalizacao') do
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")   
    @omnimais.anexar_documentos
  end
    
  Quando('realizo a assinatura dos documentos via sms e envio para formalizacao') do
    @omnimais.assinatura_digital
  end

  Quando('realizo a assinatura dos documentos via sms com seguro e envio para formalizacao') do
    @omnimais.assinatura_digital_seguro
  end

  Quando('realizo a assinatura dos documentos via sms com assistencia e envio para formalizacao') do
    @omnimais.assinatura_digital_assistencia
  end

  Quando('realizo a assinatura dos documentos via sms com seguro e assistencia e envio para formalizacao') do
    @omnimais.assinatura_digital
  end
  
  Quando('realizo a assinatura dos documentos via sms sem seguro e sem assitencia e envio para formalizacao') do
    @omnimais.assinatura_digital_nenhum
  end

  Quando('realizo a assinatura dos documentos via celular com seguro e com assistencia') do
    @omnimais.assinatura_digital_celular
  end

  Quando('realizo a assinatura dos documentos via celular com assistencia') do
    @omnimais.assinatura_digital_celular
  end

  Quando('realizo a assinatura dos documentos via celular') do
    @omnimais.assinatura_digital_celular
  end

  Quando('realizo a assinatura dos documentos via celular com seguro') do
    @omnimais.assinatura_digital_celular
  end

  Quando('realizo a assinatura dos documentos via email com seguro e com assistencia') do
    @omnimais.assinatura_digital_email
  end

  Quando('realizo a assinatura dos documentos via email com seguro') do
    @omnimais.assinatura_digital_email
  end

  Quando('realizo a assinatura dos documentos via email com assistencia') do
    @omnimais.assinatura_digital_email
  end

  Quando('realizo a assinatura dos documentos via email') do
    @omnimais.assinatura_digital_email
  end

  Quando('envio para a formalizacao') do
    @omnimais.enviar_formalizacao
  end
    
  Quando('acesso o omnifacil com usuario da formalizacao') do
    @loginomnifacil.load 
    @loginomnifacil.acessar_conta_omnifacil_mesa
  end
  
  Quando('acesso o novo menu') do
    @omnifacil.acessar_novo_menu
  end

  Quando('acesso o menu snv') do
    @omnifacil.acessar_menu_snv
  end
    
  Quando('seleciono o agente correspondente') do
    @omnifacil.listar_agente
  end
    
  Quando('acesso a fila de formalizacao') do
    @omnifacil.selecionar_menu('OPERACIONAL')
    @omnifacil.selecionar_submenu('CDC LOJA')
    @omnifacil.selecionar_submenu('Controle Documentos (Digital)')
    @omnifacil.selecionar_submenu('1 - Envio Documentos')
  end
    
  Quando('busco pelo numero do contrato gerado') do
    @omnifacil.pesquisar_contrato
  end

  Quando('pesquiso o contrato no menu snv') do
    @omnifacil.pesquisar_contrato_snv
  end

  Quando('aceito os documentos') do
    @omnifacil.botao_formalizar
    @omnifacil.aceite_documentos
  end
    
  Quando('faco a formalizacao') do
    @omnifacil.pesquisar_contrato_snv
    @omnifacil.acessar_contrato_snv
    @omnifacil.formalizar_contrato_snv
  end

  Entao('acesso o omnimais com o usuario de loja e verifico se o contrato esta aguardando pagamento') do
    @login.load
    @login.acessar_conta_omnimais_loja
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.acessar_aba_pagamentos
    @omnimais.pesquisar_proposta_valida
    proposta = @excecao.valida_ficha_numero
    expect((proposta).match($proposta)).to be_truthy
  end

  Entao('verifico se a proposta esta aguardando analise da mesa') do
    @excecao.acessar_aba_analise
    @omnimais.pesquisar_proposta_valida
    proposta = @excecao.valida_ficha_numero
    expect((proposta).match($proposta)).to be_truthy
  end


  