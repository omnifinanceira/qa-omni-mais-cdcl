Quando('logo como cdc premium') do
    @loginomnifacil = LoginOmniFacil.new
    @omnifacil = OmniFacil.new
    @login = Login.new
    @home = Home.new
    @omnimais = OmniMais.new
    @excecao = Excecao.new
    @login.load
    @login.acessar_conta_omnimais_premium
  end

  Quando('clico no tipo de operacao premium') do
    @omnimais.selecionar_opcao('CDC Premium')
  end

  Quando('preencho as informacoes do cliente {string} e {string} premium') do |cpf, nascimento|
    @omnimais.preencher_dados_cliente_premium(cpf,nascimento, 'RETENÇÃO VERDE PREMIUM 3 CAR 45')
    @omnimais.clicar_em_continuar
    sleep(5)
  end

  Quando('seleciono o seguro premium') do
    @omnimais.selecionar_seguro('SEGUROS MÓVEIS PREMIUM')
  end

  Quando('seleciono o codigo de operacao premium') do
    @omnimais.selecionar_operacao('8675 - CDC PREMIUM 3 S/J SNV SUL')
    @omnimais.clicar_em_continuar_entrada
  end

  Quando('seleciono a opcao de nenhum') do
    @omnimais.selecionar_opcao_nenhum('NENHUM')
  end

  Quando('acesso omnimais como usuario premium') do
    @login.load
    @login.acessar_conta_omnimais_premium
  end

  Quando('preencho os demais dados do cliente premium') do
    @omnimais.preencher_demais_dados_cliente_premium
  end

  Quando('preencho os demais dados do cliente premium assincrono') do
    @omnimais.anexar_foto
    @omnimais.adicionar_produto
    @omnimais.clicar_em_analisar
    retorno_botao = @omnimais.analise_assincrona
    expect(retorno_botao).to be_falsey
  end

  Quando('adiciono um produto') do
    @omnimais.adicionar_produto
    @omnimais.clicar_em_analisar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")    
  end

  Quando('adiciono mais de um produto') do
    @omnimais.adicionar_mais_produto
    @omnimais.clicar_em_analisar
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")    
  end

  Quando('realizo a assinatura dos documentos via celular premium e envio para formalizacao') do
    @omnimais.assinatura_digital_celular_premium
  end

  Quando('realizo a assinatura dos documentos via email premium e envio para formalizacao') do
    @omnimais.assinatura_digital_email_premium
  end

  Quando('realizo a assinatura dos documentos premium via sms e envio para formalizacao') do
    @omnimais.assinatura_digital_nenhum
  end

  Entao('acesso o omnimais com o usuario de premium e verifico se o contrato esta aguardando pagamento') do
    @login.load
    @login.acessar_conta_omnimais_premium
    @omnimais.aguardar_elemento_ficar_invisivel("//div[@id='loader']")  
    @omnimais.acessar_aba_pagamentos
    @omnimais.pesquisar_proposta_valida
    proposta = @excecao.valida_ficha_numero
    expect((proposta).match($proposta)).to be_truthy
end

  

  