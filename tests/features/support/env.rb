require 'capybara/cucumber'
require 'selenium-webdriver'
require 'site_prism'
require_relative 'helper.rb'
require 'capybara'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'pry'
require 'fileutils'
require 'allure-cucumber'
#require 'oci8'
require_relative '../pages/util.rb'
#require_relative '../support/DAO/SQLConnect.rb'
#require_relative '../support/DAO/Queries.rb'


BROWSER = ENV['BROWSER']
AMBIENTE = ENV['AMBIENTE']
CONFIG = YAML.load_file(File.dirname(__FILE__) + "/ambientes/#{AMBIENTE}.yml")
BANCO = YAML.load_file(File.dirname(__FILE__) + "/DAO/config.yml")

World(Helper)
World(Capybara::DSL)
World(Capybara::RSpecMatchers)

util = Util.new
util.criar_pasta_log

Capybara.register_driver :chrome do |app|
    caps = Selenium::WebDriver::Remote::Capabilities.chrome('goog:chromeOptions' => { args: ['--start-maximized']})
    $driver = Capybara::Selenium::Driver.new(app, browser:  :chrome, :capabilities => caps)
end
    

Capybara.configure do |config|
    config.default_driver = :chrome
    config.app_host = CONFIG['url_padrao']
    config.default_max_wait_time = 5
end

AllureCucumber.configure do |config|
    config.results_directory = "/reports_allure"
    config.clean_results_directory = true
    config.logging_level = Logger::INFO
    config.logger = Logger.new($stdout, Logger::DEBUG)
    config.environment = "staging"
    config.environment_properties = {
    custom_attribute: "WEB CDCLOJA"
}

    config.categories = File.new("categories/my_custom_categories.json")
end
